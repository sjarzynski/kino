﻿using System.Collections.Generic;

namespace Kino.Database.Models
{
    public class Customer
    {
        public Customer()
        {
            Tickets = new List<Ticket>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}
