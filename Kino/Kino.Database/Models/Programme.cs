﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kino.Database.Models
{
    public class Programme
    {
        public Programme()
        {
            PlayingHoursScreeningRooms = new List<PlayingHoursScreeningRoom>();
        }

        public int Id { get; set; }
        public int CinemaId { get; set; }

        [ForeignKey("CinemaId")] public Cinema Cinema { get; set; }

        public int MovieId { get; set; }

        [ForeignKey("MovieId")] public Movie Movie { get; set; }

        public List<PlayingHoursScreeningRoom> PlayingHoursScreeningRooms { get; set; }

        [Column(TypeName = "date")] public DateTime StartDate { get; set; }
    }
}