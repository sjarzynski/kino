﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kino.Database.Models
{
    public class PlayingHoursScreeningRoom
    {
        public PlayingHoursScreeningRoom()
        {
            PlayingHours = new List<PlayingHour>();
        }

        [Column("Id")] public int Id { get; set; }

        public int ScreeningRoomId { get; set; }

        [ForeignKey("ScreeningRoomId")] public ScreeningRoom ScreeningRoom { get; set; }

        public List<PlayingHour> PlayingHours { get; set; }
    }
}