﻿using System;

namespace Kino.Database.Models
{
    public class PlayingHour
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
    }
}