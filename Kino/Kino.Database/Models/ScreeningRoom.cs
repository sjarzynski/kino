﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Kino.Database.Models
{
    public class ScreeningRoom
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int Rows { get; set; }
        public int SeatsInRow { get; set; }
        public int CinemaId { get; set; }

        [ForeignKey("CinemaId")] public Cinema Cinema { get; set; }
    }
}