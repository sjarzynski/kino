﻿using System;
using System.Collections.Generic;

namespace Kino.Database.Models
{
    public class Cinema
    {
        public Cinema()
        {
            ScreeningRooms = new HashSet<ScreeningRoom>();
            Programmes = new HashSet<Programme>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime OpenHours { get; set; }
        public DateTime CloseHours { get; set; }
        public ICollection<ScreeningRoom> ScreeningRooms { get; set; }
        public ICollection<Programme> Programmes { get; set; }
    }
}