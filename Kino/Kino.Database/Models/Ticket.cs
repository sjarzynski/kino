﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Database.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public Movie Movie { get; set; }
        public Cinema Cinema { get; set; }
        public ScreeningRoom ScreeningRoom { get; set; }
        public int Row { get; set; }
        public int Seat { get; set; }
        public double Price { get; set; }
        public PlayingHour PlayingHour { get; set; }
        public DateTime Date { get; set; }
    }
}
