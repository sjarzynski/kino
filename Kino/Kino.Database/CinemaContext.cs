﻿using System.Data.Entity;
using Kino.Database.Models;

namespace Kino.Database
{
    public class CinemaContext : DbContext
    {
        public CinemaContext() : base("KinoDBConnectionString")
        {
        }

        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<PlayingHour> PlayingHours { get; set; }
        public DbSet<PlayingHoursScreeningRoom> PlayingHoursScreeningRooms { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<ScreeningRoom> ScreeningRooms { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
    }
}