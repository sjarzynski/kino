﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Kino.Database;
using Kino.Database.Models;

namespace Kino.BusinessLogic
{
    public class TicketService : ITicketService
    {
        private readonly ICinemaService _cinemaService;
        private readonly IMovieService _movieService;
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly ICustomerService _customerService;
        private readonly IProgrammeService _programmeService;
        private readonly IDiscountService _discountService;

        public TicketService(ICinemaService cinemaService,
                            IMovieService movieService,
                            IScreeningRoomService screeningRoomService,
                            ICustomerService customerService,
                            IProgrammeService programmeService,
                            IDiscountService discountService)
        {
            _cinemaService = cinemaService;
            _movieService = movieService;
            _screeningRoomService = screeningRoomService;
            _customerService = customerService;
            _programmeService = programmeService;
            _discountService = discountService;
        }

        public PagedResults<TicketBl> GetTicketsHistoryPage(string customerEmail, int pageSize, int page)
        {
            var allTicketsOfSingleCustomer = GetAllTicketsOfSingleCustomerAsync(customerEmail).Result.ToList();
            var resultsCount = allTicketsOfSingleCustomer.Count();

            var pagesCount = (int)Math.Ceiling((double)resultsCount / (double)pageSize);

            var resultItems = allTicketsOfSingleCustomer
                .Skip(pageSize * (page - 1))
                .Take(pageSize)
                .Select(x => new TicketBl {
                    Id = x.Id,
                    Movie = x.Movie,
                    Cinema = x.Cinema,
                    ScreeningRoom = x.ScreeningRoom,
                    Customer = x.Customer,
                    Date = x.Date,
                    PlayingHour = x.PlayingHour,
                    Row = x.Row,
                    Seat = x.Seat,
                    Price = x.Price
                })
                .ToList();

            var results = new PagedResults<TicketBl>
            {
                Page = page,
                PageSize = pageSize,
                ResultsCount = resultsCount,
                PagesCount = pagesCount,
                NextPageAttributes = (pagesCount <= page)
                    ? null
                    : $"&pageSize={pageSize}&page={page + 1}",
                PrevPageAttributes = (page <= 1)
                    ? null
                    : $"&pageSize={pageSize}&page={page - 1}",
                Results = resultItems
            };

            return results;
        }

        public bool CheckIfTicketHasAllNecessaryProperties(TicketBl ticketBl)
        {
            bool doesCinemaExist = _cinemaService.CheckIfCinemaExistsInDatabaseAsync(ticketBl.Cinema.Name).Result;
            if (doesCinemaExist)
            {
                ticketBl.Cinema = _cinemaService.GetCinemaAsync(ticketBl.Cinema.Name).Result;
            }
            else
            {
                return false;
            }

            bool doesCustomerExist = _customerService.CheckIfCustomerExistsInDatabaseAsync(ticketBl.Customer).Result;
            if (!doesCustomerExist)
                return false;

            DateTime startDate = _programmeService.SetCurrentStartDate(ticketBl.Date);
            List<ProgrammeBl> programmesBl = _programmeService.GetProgrammesForCinemaAsync(startDate, ticketBl.Cinema).Result;
            if (programmesBl.Count == 0)
                return false;

            string movieShow = ticketBl.Movie.Title + "/" + ticketBl.ScreeningRoom.Number.ToString() + "/" +
                               ticketBl.PlayingHour.Time.Hour.ToString("00.##") + ":" + ticketBl.PlayingHour.Time.Minute.ToString("00.##");

            ticketBl = _programmeService.GetMovieScreeningRoomAndPlayingHour(movieShow, programmesBl, ticketBl);
            if (ticketBl.Movie == null)
                return false;

            bool isRowCorrect = _screeningRoomService.CheckIfRowIsCorrect(ticketBl.Row, ticketBl.ScreeningRoom);
            if (!isRowCorrect)
                return false;

            bool isSeatCorrect = _screeningRoomService.CheckIfSeatIsCorrect(ticketBl.Seat, ticketBl.ScreeningRoom);
            if (!isSeatCorrect)
                return false;

            return true;
        }

        public async Task<IEnumerable<TicketBl>> GetAllTicketsAsync()
        {
            using (var ctx = new CinemaContext())
            {
                var tickets = (await ctx.Tickets.ToListAsync()).Select(Mapper.Map<Ticket, TicketBl>);
                return tickets;
            }
        }

        public async Task<IEnumerable<TicketBl>> GetAllTicketsOfSingleCustomerAsync(string customerEmail)
        {
            using (var ctx = new CinemaContext())
            {
                var tickets = (await ctx.Tickets.Include(x=>x.Cinema)
                                         .Include(x=>x.Customer)
                                         .Include(x=>x.Movie)
                                         .Include(x=>x.PlayingHour)
                                         .Include(x=>x.ScreeningRoom)
                                         .Where(x=>x.Customer.Email == customerEmail)
                                         .ToListAsync())
                                         .Select(Mapper.Map<Ticket, TicketBl>);
                return tickets;
            }
        }

        public async Task<bool> CheckIfTicketExistAsync(TicketBl ticketBl)
        {
            var ticket = Mapper.Map<TicketBl, Ticket>(ticketBl);
            Ticket ticketInDb;

            using (var ctx = new CinemaContext())
            {
                ticketInDb = await ctx.Tickets.FirstOrDefaultAsync(t => t.Cinema.Name == ticket.Cinema.Name
                                                                 && t.Movie.Title == ticket.Movie.Title
                                                                 && t.Date.Year == ticket.Date.Year
                                                                 && t.Date.Month == ticket.Date.Month
                                                                 && t.Date.Day == ticket.Date.Day
                                                                 && t.ScreeningRoom.Number == ticket.ScreeningRoom.Number
                                                                 && t.PlayingHour.Time.Hour == ticket.PlayingHour.Time.Hour
                                                                 && t.PlayingHour.Time.Minute == ticket.PlayingHour.Time.Minute
                                                                 && t.Row == ticket.Row
                                                                 && t.Seat == ticket.Seat);
            }

            if (ticketInDb == null)
            {
                return false;
            }

            return true;
        }

        public async Task<int> AddTicketAsync(TicketBl ticketBl)
        {
            var ticket = Mapper.Map<TicketBl, Ticket>(ticketBl);

            using (var ctx = new CinemaContext())
            {
                ticket.Cinema = await ctx.Cinemas.Include(x => x.ScreeningRooms)
                    .FirstOrDefaultAsync(x => x.Name == ticket.Cinema.Name);
                ticket.Movie = await ctx.Movies.FirstOrDefaultAsync(x => x.Title == ticket.Movie.Title);
                ticket.ScreeningRoom =
                    await ctx.ScreeningRooms.FirstOrDefaultAsync(x => x.Id == ticket.ScreeningRoom.Id);
                ticket.Customer =
                    await ctx.Customers.FirstOrDefaultAsync(x => x.Email == ticket.Customer.Email);
                ticket.PlayingHour = await ctx.PlayingHours.FirstOrDefaultAsync(x => x.Id == ticket.PlayingHour.Id);

                ctx.Tickets.Add(ticket);
                await ctx.SaveChangesAsync();
            }

            return ticket.Id;
        }

        public double GetTicketPrice(TicketBl ticketBl)
        {
            double price = 20;

            if (ticketBl.Date.DayOfWeek == DayOfWeek.Friday
                || ticketBl.Date.DayOfWeek == DayOfWeek.Saturday
                || ticketBl.Date.DayOfWeek == DayOfWeek.Sunday)
            {
                price = 25;
            }

            var tickets = GetAllTicketsOfSingleCustomerAsync(ticketBl.Customer.Email).Result.ToList();
            if (tickets == null)
            {
                return price;
            }

            price = _discountService.ApplyMonthlyDiscount(price, tickets);
            price = _discountService.ApplyDiscountToTenthTicket(price, tickets);

            return price;
        }

        public double SumTicketsPrice(double sumPrices, double singlePrice)
        {
            sumPrices = sumPrices + singlePrice;
            return sumPrices;
        }

        public TicketBl DuplicateTicket(TicketBl ticketBl)
        {
            var duplicatedTicket = Mapper.Map<TicketBl, TicketBl>(ticketBl);
            return duplicatedTicket;
        }
    }
}
