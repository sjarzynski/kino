﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Kino.Database;
using Kino.Database.Models;

namespace Kino.BusinessLogic
{
    public class CinemaService : ICinemaService
    {
        private readonly IParser _parser;

        public CinemaService(IParser parser)
        {
            _parser = parser;
        }

        public bool CheckIfCinemaHasAllNecessaryProperties(CinemaBl cinema)
        {
            if (string.IsNullOrEmpty(cinema.Name))
                return false;

            if (string.IsNullOrEmpty(cinema.Address))
                return false;

            if (cinema.OpenHours == DateTime.MinValue)
                return false;

            if (cinema.CloseHours == DateTime.MinValue)
                return false;

            var isPhoneNumberCorrect = _parser.ParsePhoneNumberAnswer(cinema.PhoneNumber);
            if (!isPhoneNumberCorrect)
                return false;

            var isEmailCorrect = _parser.ParseEmailAnswer(cinema.Email);
            if (!isEmailCorrect)
                return false;

            return true;
        }

        public async Task<int> AddCinemaAsync(string name, string address, string eMail, string phoneNumber,
            DateTime openHours, DateTime closeHours)
        {
            var cinema = new Cinema
            {
                Name = name,
                Address = address,
                Email = eMail,
                PhoneNumber = phoneNumber,
                OpenHours = openHours,
                CloseHours = closeHours
            };

            using (var ctx = new CinemaContext())
            {
                ctx.Cinemas.Add(cinema);
                await ctx.SaveChangesAsync();
            }

            return cinema.Id;
        }

        public async Task<bool> CheckIfCinemaExistsInDatabaseAsync(string cinemaName)
        {
            Cinema cinema;

            using (var ctx = new CinemaContext())
            {
                cinema = await ctx.Cinemas.FirstOrDefaultAsync(c => c.Name == cinemaName);
            }

            return cinema != null;
        }

        public async Task<bool> CheckIfThereIsScreeningRoomInTheCinemaAsync(string cinemaName, int number)
        {
            Cinema cinema;

            using (var ctx = new CinemaContext())
            {
                cinema = await ctx.Cinemas
                    .Include(x => x.ScreeningRooms)
                    .FirstOrDefaultAsync(c => c.Name == cinemaName);
            }

            return cinema != null && cinema.ScreeningRooms.Any(screeningRoom => screeningRoom.Number == number);
        }

        public async Task<IEnumerable<CinemaBl>> GetAllCinemasAsync()
        {
            using (var ctx = new CinemaContext())
            {
                var cinemas = (await ctx.Cinemas.ToListAsync()).Select(Mapper.Map<Cinema, CinemaBl>);
                return cinemas;
            }
        }

        public async Task<CinemaBl> GetCinemaAsync(string name)
        {
            Cinema cinema;

            using (var ctx = new CinemaContext())
            {
                cinema = await ctx.Cinemas.FirstOrDefaultAsync(c => c.Name == name);
            }

            var cinemaBl = Mapper.Map<Cinema, CinemaBl>(cinema);
            return cinemaBl;
        }

        public async Task<ScreeningRoomBl> GetScreeningRoomForTheCinemaAsync(CinemaBl cinemaBl, int number)
        {
            Cinema cinema;

            using (var ctx = new CinemaContext())
            {
                cinema = await ctx.Cinemas
                    .Include(x => x.ScreeningRooms)
                    .FirstOrDefaultAsync(c => c.Name == cinemaBl.Name);
            }

            if (cinema != null)
                foreach (var screeningRoom in cinema.ScreeningRooms)
                    if (screeningRoom.Number == number)
                    {
                        var screeningRoomBl = Mapper.Map<ScreeningRoom, ScreeningRoomBl>(screeningRoom);
                        return screeningRoomBl;
                    }

            return null;
        }
    }
}