﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExtendedXmlSerializer.Configuration;
using ExtendedXmlSerializer.ExtensionModel.Xml;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Kino.Database;
using Kino.Database.Models;
using Newtonsoft.Json;

namespace Kino.BusinessLogic
{
    public class ProgrammeService : IProgrammeService
    {
        private readonly IParser _parser;
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly TimeSpan _spaceBetweenMovies = new TimeSpan(0, 0, 10, 0);

        public ProgrammeService(IParser parser, IScreeningRoomService screeningRoomService)
        {
            _parser = parser;
            _screeningRoomService = screeningRoomService;
        }

        public TicketBl GetMovieScreeningRoomAndPlayingHour(string movieShow, List<ProgrammeBl> programmesBl, TicketBl ticketBl)
        {
            string[] splitWords = movieShow.Split('/');
            if (splitWords.Length != 3)
            {
                return ticketBl;
            }

            string movieTitle = splitWords[0];
            bool isScreeningRoomAnInt = int.TryParse(splitWords[1], out int screeningRoomNumber);

            var provider = CultureInfo.InvariantCulture;
            const string format = "HH:mm";
            bool isPlayingHourADateTime =  DateTime.TryParseExact(splitWords[2], format, provider, DateTimeStyles.None, out var playingHour);

            foreach (var programmeBl in programmesBl)
            {
                if (programmeBl.Movie.Title.ToUpper() == movieTitle.ToUpper())
                {
                    foreach (var phsr in programmeBl.PlayingHoursScreeningRooms)
                    {
                        if (phsr.ScreeningRoom.Number == screeningRoomNumber)
                        {
                            foreach (var ph in phsr.PlayingHours)
                            {
                                if (ph.Time.ToShortTimeString() == playingHour.ToShortTimeString())
                                {
                                    ticketBl.Movie = programmeBl.Movie;
                                    ticketBl.ScreeningRoom = phsr.ScreeningRoom;
                                    ticketBl.PlayingHour = ph;
                                    return ticketBl;
                                }
                            }
                        }
                    }
                }
            }

            return ticketBl;
        }

        public async Task<int> AddProgrammeAsync(ProgrammeBl programmeBl)
        {
            var programme = new Programme
            {
                PlayingHoursScreeningRooms =
                    Mapper.Map<List<PlayingHoursScreeningRoomBl>, List<PlayingHoursScreeningRoom>>(programmeBl
                        .PlayingHoursScreeningRooms)
            };

            using (var ctx = new CinemaContext())
            {
                foreach (var phsr in programme.PlayingHoursScreeningRooms)
                    phsr.ScreeningRoom = await ctx.ScreeningRooms
                        .Where(v => v.Cinema.Name == programmeBl.Cinema.Name)
                        .FirstOrDefaultAsync(c => c.Number == phsr.ScreeningRoom.Number);

                programme.Cinema = await ctx.Cinemas.Include(x => x.ScreeningRooms)
                    .FirstOrDefaultAsync(c => c.Name == programmeBl.Cinema.Name);
                programme.Movie = await ctx.Movies.FirstOrDefaultAsync(m => m.Title == programmeBl.Movie.Title);
                programme.StartDate = programmeBl.StartDate;

                ctx.Programmes.Add(programme);
                await ctx.SaveChangesAsync();
            }

            return programme.Id;
        }

        public bool CheckIfDateIsEmpty(DateTime date)
        {
            return date.Year <= 1;
        }

        public async Task<IEnumerable<ProgrammeBl>> GetAllProgrammesAsync(string cinema)
        {
            List<Programme> programmes;
            var playingHoursScreeningRooms = new List<PlayingHoursScreeningRoom>();

            using (var ctx = new CinemaContext())
            {
                programmes = await ctx.Programmes.Where(c => c.Cinema.Name == cinema)
                    .Include(x => x.Cinema)
                    .Include(y => y.Movie)
                    .Include(z => z.PlayingHoursScreeningRooms)
                    .ToListAsync();

                playingHoursScreeningRooms = await ctx.PlayingHoursScreeningRooms
                    .Include(x => x.PlayingHours)
                    .Include(y => y.ScreeningRoom)
                    .ToListAsync();
            }

            var programmesBl = Mapper.Map<List<Programme>, List<ProgrammeBl>>(programmes);
            return programmesBl;
        }

        public string GetDateRange(DateTime firstDay)
        {
            const int weekWithoutTheSameDay = 6;
            var lastDay = firstDay.AddDays(weekWithoutTheSameDay);

            return firstDay.ToShortDateString() + " - " + lastDay.ToShortDateString();
        }

        public async Task<DateTime> GetLastStartDateAsync(CinemaBl cinema, MovieBl movie)
        {
            var dateTime = new DateTime(0001, 01, 01, 01, 01, 01);
            Programme programme;

            using (var ctx = new CinemaContext())
            {
                programme = await ctx.Programmes
                    .Where(c => c.Cinema.Name == cinema.Name)
                    .Where(m => m.Movie.Title == movie.Title)
                    .Include(x => x.Cinema)
                    .Include(y => y.Movie)
                    .OrderByDescending(z => z.StartDate)
                    .FirstOrDefaultAsync();
            }

            if (programme != null) return programme.StartDate;

            return dateTime;
        }

        public DateTime GetNextStartDate(DateTime date)
        {
            const int theSameDayNextWeek = 7;

            var followingWeek = date.AddDays(theSameDayNextWeek);

            return followingWeek;
        }

        public async Task<List<ProgrammeBl>> GetProgrammesForCinemaAsync(DateTime startDate, CinemaBl cinema)
        {
            List<Programme> programmes;
            var playingHoursScreeningRooms = new List<PlayingHoursScreeningRoom>();

            using (var ctx = new CinemaContext())
            {
                programmes = await ctx.Programmes
                    .Where(c => c.Cinema.Name == cinema.Name)
                    .Where(sd => DbFunctions.TruncateTime(sd.StartDate) == startDate.Date)
                    .Include(x => x.Cinema)
                    .Include(y => y.Movie)
                    .Include(z => z.PlayingHoursScreeningRooms)
                    .ToListAsync();

                playingHoursScreeningRooms = await ctx.PlayingHoursScreeningRooms
                    .Include(x => x.PlayingHours)
                    .Include(y => y.ScreeningRoom)
                    .ToListAsync();
            }

            var programmesBl = Mapper.Map<List<Programme>, List<ProgrammeBl>>(programmes);

            return programmesBl;
        }

        public void PrintToJson(IEnumerable<ProgrammeBl> programmes)
        {
            try
            {
                var json = JsonConvert.SerializeObject(programmes);
                File.WriteAllText(@".\programme.json", json);
            }
            catch
            {
            }
        }

        public void PrintToXml(IEnumerable<ProgrammeBl> programmes)
        {
            try
            {
                var serializer = new ConfigurationContainer().UseOptimizedNamespaces().Create();
                var xml = serializer.Serialize(programmes);
                File.WriteAllText(@".\programme.xml", xml);
            }
            catch
            {
            }
        }

        public DateTime SetCurrentStartDate(DateTime todayDate)
        {
            var dummyDate = new DateTime(0001, 01, 01, 01, 01, 01);
            var todayDay = todayDate.DayOfWeek;

            if (todayDay == DayOfWeek.Friday) return todayDate;

            for (double i = -1; i >= -10; i--)
            {
                var previousDate = todayDate.AddDays(i);
                if (previousDate.DayOfWeek == DayOfWeek.Friday) return previousDate;
            }

            return dummyDate;
        }

        public DateTime SetCurrentStartDate()
        {
            var dummyDate = new DateTime(0001, 01, 01, 01, 01, 01);
            var todayDate = DateTime.Now.Date;
            var todayDay = todayDate.DayOfWeek;

            if (todayDay == DayOfWeek.Friday) return todayDate;

            for (double i = -1; i >= -10; i--)
            {
                var previousDate = todayDate.AddDays(i);
                if (previousDate.DayOfWeek == DayOfWeek.Friday) return previousDate;
            }

            return dummyDate;
        }

        public bool CheckIfHourIsTooEarlyOrTooLate(CinemaBl cinema, PlayingHourBl playingHour)
        {
            var openHour = cinema.OpenHours.TimeOfDay;
            var closeHour = cinema.CloseHours.TimeOfDay;
            var movieHour = playingHour.Time.TimeOfDay;

            if (TimeSpan.Compare(openHour, movieHour) == 1)
                return true;


            if (TimeSpan.Compare(movieHour, closeHour) == 1)
                return true;

            return false;
        }

        public bool CheckIfHourIsOccupied(ProgrammeBl programme, ScreeningRoomBl screeningRoom,
            PlayingHourBl playingHour)
        {
            var programmes = GetAllProgrammesAsync(programme.Cinema.Name).Result.ToList();

            programmes.Add(programme); //Method has to check movies in current programme as well

            var thisMovieStartTime = playingHour.Time.TimeOfDay;
            thisMovieStartTime = thisMovieStartTime - _spaceBetweenMovies; //Subtract given minutes from the beginning
            //to make sure there's space between movies
            var thisMovieRuntime = TimeSpan.FromMinutes(programme.Movie.Runtime);
            var thisMovieEndTime = thisMovieStartTime + thisMovieRuntime;
            thisMovieEndTime = thisMovieEndTime + _spaceBetweenMovies; //Add given minutes to the end
            //to make sure there's space between movies

            foreach (var singleProgramme in programmes)
            {
                if (singleProgramme.StartDate != programme.StartDate) continue;

                var playingHoursScreeningRooms = singleProgramme.PlayingHoursScreeningRooms;
                foreach (var phsr in playingHoursScreeningRooms)
                {
                    if (phsr.ScreeningRoom.Number != screeningRoom.Number) continue;

                    foreach (var otherMoviePlayingHour in phsr.PlayingHours)
                    {
                        var otherMovieStartTime = otherMoviePlayingHour.Time.TimeOfDay;
                        var otherMovieRuntime = TimeSpan.FromMinutes(singleProgramme.Movie.Runtime);
                        var otherMovieEndTime = otherMovieStartTime + otherMovieRuntime;

                        if (thisMovieStartTime > otherMovieStartTime && thisMovieStartTime < otherMovieEndTime)
                            return true;

                        if (thisMovieEndTime > otherMovieStartTime && thisMovieEndTime < otherMovieEndTime) return true;
                    }
                }
            }

            return false;
        }

        public bool CheckIfProgrammeHasAllNecessaryProperties(ProgrammeBl programme)
        {
            if (string.IsNullOrEmpty(programme.Cinema.Name))
                return false;

            if (string.IsNullOrEmpty(programme.Movie.Title))
                return false;

            var isDateCorrect = _parser.ParseDateAnswer(programme.StartDate.ToShortDateString());
            if (!isDateCorrect)
                return false;

            var isPlayingHoursScreeningRoomsCorrect = programme.PlayingHoursScreeningRooms.Any();
            if (!isPlayingHoursScreeningRoomsCorrect)
                return false;

            foreach (var phsr in programme.PlayingHoursScreeningRooms)
            {
                var isScreeningRoom = _screeningRoomService.CheckIfCinemaHasAnyScreeningRoomAsync(programme.Cinema)
                    .Result;
                if (!isScreeningRoom)
                    return false;

                var isPlayingHoursCorrect = phsr.PlayingHours.Any();
                if (!isPlayingHoursCorrect)
                    return false;

                foreach (var ph in phsr.PlayingHours)
                {
                    var isPlayingHourCorrect = _parser.ParseHourAnswer(ph.Time.ToLongTimeString());
                    if (!isPlayingHourCorrect)
                        return false;
                }
            }

            return true;
        }
    }
}