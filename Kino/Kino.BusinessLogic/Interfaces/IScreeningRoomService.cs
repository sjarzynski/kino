﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IScreeningRoomService
    {
        Task<int> AddScreeningRoomAsync(CinemaBl cinemaBl, int number, int rows, int seatsInRow);
        Task<bool> CheckIfCinemaHasAnyScreeningRoomAsync(CinemaBl cinemaBl);
        Task<List<ScreeningRoomBl>> GetAllScreeningRoomsInOneCinemaAsync(CinemaBl cinemaBl);
        bool CheckIfScreeningRoomHasAllNecessaryProperties(ScreeningRoomBl sr);
        Task<bool> CheckIfThisScreeningRoomExistInCinema(CinemaBl cinemaBl, int screeningRoomNumber);
        bool CheckIfRowIsCorrect(int row, ScreeningRoomBl screeningRoom);
        bool CheckIfSeatIsCorrect(int seat, ScreeningRoomBl screeningRoom);
    }
}