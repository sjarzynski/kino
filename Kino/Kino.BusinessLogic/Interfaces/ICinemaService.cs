﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface ICinemaService
    {
        Task<int> AddCinemaAsync(string name, string address, string eMail, string phoneNumber, DateTime openHours,
            DateTime closeHours);
        Task<bool> CheckIfCinemaExistsInDatabaseAsync(string cinemaName);
        bool CheckIfCinemaHasAllNecessaryProperties(CinemaBl cinema);
        Task<bool> CheckIfThereIsScreeningRoomInTheCinemaAsync(string cinemaName, int number);
        Task<IEnumerable<CinemaBl>> GetAllCinemasAsync();
        Task<CinemaBl> GetCinemaAsync(string name);
        Task<ScreeningRoomBl> GetScreeningRoomForTheCinemaAsync(CinemaBl cinema, int number);
    }
}