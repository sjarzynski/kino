﻿using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface ICustomerService
    {
        Task<bool> CheckIfCustomerExistsInDatabaseAsync(CustomerBl customerBl);
        Task<int> AddCustomerAsync(CustomerBl customerBl);
        Task<CustomerBl> GetCustomerAsync(CustomerBl customerBl);
        bool CheckIfCustomerHasAllNecessaryProperties(CustomerBl customerBl);
        string CreateMD5(string input);
        bool IsValidCustomer(string eMail, CustomerBl customerBl);
    }
}