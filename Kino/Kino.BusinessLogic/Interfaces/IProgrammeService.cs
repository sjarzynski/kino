﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IProgrammeService
    {
        Task<int> AddProgrammeAsync(ProgrammeBl programmeBl);
        bool CheckIfDateIsEmpty(DateTime date);
        bool CheckIfProgrammeHasAllNecessaryProperties(ProgrammeBl programme);
        Task<IEnumerable<ProgrammeBl>> GetAllProgrammesAsync(string cinema);
        string GetDateRange(DateTime firstDay);
        Task<DateTime> GetLastStartDateAsync(CinemaBl cinema, MovieBl movie);
        DateTime GetNextStartDate(DateTime date);
        Task<List<ProgrammeBl>> GetProgrammesForCinemaAsync(DateTime startDate, CinemaBl cinema);
        void PrintToJson(IEnumerable<ProgrammeBl> programmes);
        void PrintToXml(IEnumerable<ProgrammeBl> programmes);
        DateTime SetCurrentStartDate();
        DateTime SetCurrentStartDate(DateTime todayDate);
        bool CheckIfHourIsTooEarlyOrTooLate(CinemaBl cinema, PlayingHourBl playingHour);
        bool CheckIfHourIsOccupied(ProgrammeBl programme, ScreeningRoomBl screeningRoom, PlayingHourBl playingHour);
        TicketBl GetMovieScreeningRoomAndPlayingHour(string movieShow, List<ProgrammeBl> programmesBl, TicketBl ticketBl);
    }
}