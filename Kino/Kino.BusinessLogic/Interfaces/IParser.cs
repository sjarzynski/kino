﻿using System;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IParser
    {
        bool CheckIfDateIsFriday(string textFromUser);
        DateTime ConvertStringToHoursAndMinutes(string userAnswer);
        DateTime GetDateFromString(string text);
        int ParseChosenOption(string option);
        bool ParseDateAnswer(string textFromUser);
        bool ParseEmailAnswer(string textFromUser);
        bool ParseFileExtensionAnswer(string textFromUser);
        bool ParseGenreAnswer(string textFromUser);
        int ParseIntegerAnswer(string textFromUser);
        bool ParseOpenAnswer(string textFromUser);
        bool ParsePhoneNumberAnswer(string textFromUser);
        bool ParseHourAnswer(string textFromUser);
        int ParseScreeningRoomNumber(string textFromUser);
        bool ParseWebsiteAnswer(string textFromUser);
        bool? ParseYesNoAnswer(string textFromUser);
        DateTime GetStartDate(DateTime date);
    }
}