﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface ITicketService
    {
        Task<IEnumerable<TicketBl>> GetAllTicketsAsync();
        Task<bool> CheckIfTicketExistAsync(TicketBl ticketBl);
        Task<int> AddTicketAsync(TicketBl ticketBl);
        double GetTicketPrice(TicketBl ticketBl);
        Task<IEnumerable<TicketBl>> GetAllTicketsOfSingleCustomerAsync(string customerEmail);
        bool CheckIfTicketHasAllNecessaryProperties(TicketBl ticketBl);
        double SumTicketsPrice(double sumPrices, double singlePrice);
        TicketBl DuplicateTicket(TicketBl ticketBl);
        PagedResults<TicketBl> GetTicketsHistoryPage(string customerEmail, int pageSize, int page);
    }
}
