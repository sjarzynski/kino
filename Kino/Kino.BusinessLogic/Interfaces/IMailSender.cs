﻿using System.Collections.Generic;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IMailSender
    {
        bool SendMail(List<TicketBl> tickets, double sumOfPrices);
    }
}