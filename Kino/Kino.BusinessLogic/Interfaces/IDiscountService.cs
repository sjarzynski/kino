﻿using Kino.BusinessLogic.Models;
using System.Collections.Generic;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IDiscountService
    {
        double ApplyDiscountToTenthTicket(double price, List<TicketBl> tickets);
        double ApplyMonthlyDiscount(double price, List<TicketBl> tickets);
    }
}