﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Interfaces
{
    public interface IMovieService
    {
        Task<int> AddMovieAsync(string title, string description, string genre, int runtime, string webUrl);
        bool CheckIfMovieHasAllNecessaryProperties(MovieBl movie);
        Task<bool> CheckIfMovieExistsInDatabaseAsync(string movieTitle);
        Task<IEnumerable<MovieBl>> GetAllMoviesAsync();
        Task<MovieBl> GetMovieAsync(string title);
    }
}