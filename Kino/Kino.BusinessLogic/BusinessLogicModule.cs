﻿using Kino.BusinessLogic.Helpers;
using Kino.BusinessLogic.Interfaces;
using Ninject.Modules;

namespace Kino.BusinessLogic
{
    public class BusinessLogicModule : NinjectModule
    {
        public override void Load()
        {
            if (Kernel == null) return;

            Kernel.Bind<IMovieService>().To<MovieService>();
            Kernel.Bind<ICinemaService>().To<CinemaService>();
            Kernel.Bind<IScreeningRoomService>().To<ScreeningRoomService>();
            Kernel.Bind<IParser>().To<Parser>();
            Kernel.Bind<IProgrammeService>().To<ProgrammeService>();
            Kernel.Bind<ICustomerService>().To<CustomerService>();
            Kernel.Bind<ITicketService>().To<TicketService>();
            Kernel.Bind<IMailSender>().To<MailSender>();
            Kernel.Bind<IDiscountService>().To<DiscountService>();
        }
    }
}