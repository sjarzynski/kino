﻿using System;

namespace Kino.BusinessLogic.Models
{
    public class PlayingHourBl
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
    }
}