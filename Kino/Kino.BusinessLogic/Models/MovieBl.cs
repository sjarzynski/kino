﻿using System.Collections.Generic;

namespace Kino.BusinessLogic.Models
{
    public class MovieBl
    {
        public enum Genre
        {
            Action,
            Adult,
            Adventure,
            Experimental,
            Family,
            Comedy,
            ComedyDrama,
            Crime,
            Drama,
            Epic,
            Fantasy,
            HistoricalFilm,
            Horror,
            Musical,
            Mystery,
            Romance,
            ScienceFiction,
            SpyFilm,
            Thriller,
            War,
            Western
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Genre MovieGenre { get; set; }
        public int Runtime { get; set; }
        public string Description { get; set; }
        public string WebUrl { get; set; }
    }
}