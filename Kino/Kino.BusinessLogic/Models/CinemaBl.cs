﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Kino.BusinessLogic.Models
{
    public class CinemaBl
    {
        public CinemaBl()
        {
            ScreeningRooms = new HashSet<ScreeningRoomBl>();
            Programmes = new HashSet<ProgrammeBl>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime OpenHours { get; set; }
        public DateTime CloseHours { get; set; }

        [XmlIgnore] [JsonIgnore] public ICollection<ScreeningRoomBl> ScreeningRooms { get; set; }

        [XmlIgnore] [JsonIgnore] public ICollection<ProgrammeBl> Programmes { get; set; }
    }
}