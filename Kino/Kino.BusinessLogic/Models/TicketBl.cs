﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.BusinessLogic.Models
{
    public class TicketBl
    {
        public int Id { get; set; }
        public CustomerBl Customer { get; set; }
        public MovieBl Movie { get; set; }
        public CinemaBl Cinema { get; set; }
        public ScreeningRoomBl ScreeningRoom { get; set; }
        public int Row { get; set; }
        public int Seat { get; set; }
        public double Price { get; set; }
        public PlayingHourBl PlayingHour { get; set; }
        public DateTime Date { get; set; }
    }
}
