﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Kino.BusinessLogic.Models
{
    public class CustomerBl
    {
        public CustomerBl()
        {
            Tickets = new List<TicketBl>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        [XmlIgnore] [JsonIgnore]  public List<TicketBl> Tickets { get; set; }
    }
}
