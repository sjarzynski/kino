﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace Kino.BusinessLogic.Models
{
    public class PlayingHoursScreeningRoomBl
    {
        public PlayingHoursScreeningRoomBl()
        {
            PlayingHours = new List<PlayingHourBl>();
        }

        [Column("Id")] public int Id { get; set; }

        public int ScreeningRoomId { get; set; }

        [XmlIgnore]
        [ForeignKey("ScreeningRoomId")]
        public ScreeningRoomBl ScreeningRoom { get; set; }

        public List<PlayingHourBl> PlayingHours { get; set; }
    }
}