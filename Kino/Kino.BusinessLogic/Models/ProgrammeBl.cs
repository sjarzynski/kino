﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kino.BusinessLogic.Models
{
    public class ProgrammeBl
    {
        public ProgrammeBl()
        {
            PlayingHoursScreeningRooms = new List<PlayingHoursScreeningRoomBl>();
        }

        public int Id { get; set; }
        public int CinemaId { get; set; }

        [ForeignKey("CinemaId")] public CinemaBl Cinema { get; set; }

        public int MovieId { get; set; }

        [ForeignKey("MovieId")] public MovieBl Movie { get; set; }

        public List<PlayingHoursScreeningRoomBl> PlayingHoursScreeningRooms { get; set; }

        [Column(TypeName = "date")] public DateTime StartDate { get; set; }
    }
}