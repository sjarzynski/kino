﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace Kino.BusinessLogic.Models
{
    public class ScreeningRoomBl
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int Rows { get; set; }
        public int SeatsInRow { get; set; }
        public int CinemaId { get; set; }

        [XmlIgnore]
        [ForeignKey("CinemaId")] public CinemaBl Cinema { get; set; }
    }
}