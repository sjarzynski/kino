﻿using Kino.Database;
using Kino.Database.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using Kino.BusinessLogic.Models;
using Kino.BusinessLogic.Interfaces;
using AutoMapper;
using System.Text;

namespace Kino.BusinessLogic
{
    public class CustomerService : ICustomerService
    {
        private readonly IParser _parser;

        public CustomerService(IParser parser)
        {
            _parser = parser;
        }

        public bool IsValidCustomer(string eMail, CustomerBl customerBl)
        {
            var customerFromDb = GetCustomerAsync(customerBl).Result;
            if(customerFromDb == null)
            {
                return false;
            }

            if(customerBl.PasswordHash == null || customerFromDb.PasswordHash == null)
            {
                return false;
            }

            if(customerBl.Email == customerFromDb.Email
                && customerBl.PasswordHash.ToUpper() == customerFromDb.PasswordHash.ToUpper())
            {
                return true;
            }

            return false;
        }

        public string CreateMD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }

                return sb.ToString();
            }
        }

        public bool CheckIfCustomerHasAllNecessaryProperties(CustomerBl customerBl)
        {
            if (string.IsNullOrEmpty(customerBl.Email))
                return false;

            if (_parser.ParseEmailAnswer(customerBl.Email) == false)
                return false;

            if (string.IsNullOrEmpty(customerBl.FirstName))
                return false;

            if (string.IsNullOrEmpty(customerBl.LastName))
                return false;

            return true;
        }

        public async Task<CustomerBl> GetCustomerAsync(CustomerBl customerBl)
        {
            Customer customer;

            using (var ctx = new CinemaContext())
            {
                customer = await ctx.Customers.FirstOrDefaultAsync(c => c.Email == customerBl.Email);
            }

            if (customer == null)
            {
                return null;
            }

            var custBl = Mapper.Map<Customer, CustomerBl>(customer);
            return custBl;
        }


        public async Task<bool> CheckIfCustomerExistsInDatabaseAsync(CustomerBl customerBl)
        {
            Customer customer;

            using (var ctx = new CinemaContext())
            {
                customer = await ctx.Customers.FirstOrDefaultAsync(c=>c.Email == customerBl.Email);
            }

            return customer != null;
        }

        public async Task<int> AddCustomerAsync(CustomerBl customerBl)
        {
            var customer = Mapper.Map<CustomerBl, Customer>(customerBl);
                
            using (var ctx = new CinemaContext())
            {
                ctx.Customers.Add(customer);
                await ctx.SaveChangesAsync();
            }

            return customer.Id;
        }
    }
}
