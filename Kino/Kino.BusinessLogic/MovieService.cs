﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Kino.Database;
using Kino.Database.Models;

namespace Kino.BusinessLogic
{
    public class MovieService : IMovieService
    {
        private readonly IParser _parser;

        public MovieService(IParser parser)
        {
            _parser = parser;
        }

        public bool CheckIfMovieHasAllNecessaryProperties(MovieBl movie)
        {
            if (string.IsNullOrEmpty(movie.Title))
                return false;

            if (string.IsNullOrEmpty(movie.Description))
                return false;

            if (movie.Runtime <= 0)
                return false;

            var isWebUrlCorrect = _parser.ParseWebsiteAnswer(movie.WebUrl);
            if (!isWebUrlCorrect)
                return false;

            var isGenreCorrect = _parser.ParseGenreAnswer(movie.MovieGenre.ToString());
            if (!isGenreCorrect)
                return false;

            return true;
        }

        public async Task<int> AddMovieAsync(string title, string description, string genre, int runtime, string webUrl)
        {
            var movie = new Movie {Title = title, Description = description};

            var isGenreCorrect = Enum.TryParse(genre, out Movie.Genre movieGenre);

            if (isGenreCorrect) movie.MovieGenre = movieGenre;

            movie.Runtime = runtime;
            movie.WebUrl = webUrl;

            using (var ctx = new CinemaContext())
            {
                ctx.Movies.Add(movie);
                await ctx.SaveChangesAsync();
            }

            return movie.Id;
        }

        public async Task<bool> CheckIfMovieExistsInDatabaseAsync(string movieTitle)
        {
            var movie = new Movie();

            using (var ctx = new CinemaContext())
            {
                return await ctx.Movies.AnyAsync(m => m.Title == movieTitle);
            }
        }

        public async Task<IEnumerable<MovieBl>> GetAllMoviesAsync()
        {
            using (var ctx = new CinemaContext())
            {
                var movies = (await ctx.Movies.ToListAsync()).Select(Mapper.Map<Movie, MovieBl>);
                return movies;
            }
        }

        public async Task<MovieBl> GetMovieAsync(string title)
        {
            Movie movie;

            using (var ctx = new CinemaContext())
            {
                movie = await ctx.Movies.FirstOrDefaultAsync(m => m.Title == title);
            }

            var movieBl = Mapper.Map<Movie, MovieBl>(movie);
            return movieBl;
        }
    }
}