﻿using System;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using Kino.BusinessLogic.Interfaces;
using Kino.Database.Models;

namespace Kino.BusinessLogic
{
    public class Parser : IParser
    {
        public bool CheckIfDateIsFriday(string textFromUser)
        {
            var date = Convert.ToDateTime(textFromUser);

            return date.DayOfWeek == DayOfWeek.Friday;
        }

        public DateTime ConvertStringToHoursAndMinutes(string userAnswer)
        {
            var hour = 00;
            var minutes = 00;
            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;
            var day = DateTime.Now.Day;

            try
            {
                var splitWords = userAnswer.Split(':');
                hour = Convert.ToInt32(splitWords[0]);
                minutes = Convert.ToInt32(splitWords[1]);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            var time = new DateTime(year, month, day, hour, minutes, 0);
            return time;
        }

        public DateTime GetDateFromString(string text)
        {
            var isCorrect = DateTime.TryParse(text, out var date);

            return date;
        }

        public int ParseChosenOption(string option)
        {
            var success = int.TryParse(option, out var result);
            return result;
        }

        public bool ParseDateAnswer(string textFromUser)
        {
            var provider = CultureInfo.InvariantCulture;
            const string format = "M/d/yyyy";
            var result =
                DateTime.TryParseExact(textFromUser, format, provider, DateTimeStyles.None, out var parsedDate);

            return result;
        }

        public bool ParseEmailAnswer(string textFromUser)
        {
            var regex = new Regex(
                @"^(?<username>[\w_\.-]{2,})@(?<domain>[\dA-z]{2,})\.((?<postfix>[\dA-z]{3})\.)?(?<country>[\dA-z]{2,3})$");
            var match = regex.Match(textFromUser);

            return match.Success;
        }

        public bool ParseFileExtensionAnswer(string textFromUser)
        {
            textFromUser = textFromUser.ToUpper();

            return textFromUser == "JSON" || textFromUser == "XML";
        }

        public bool ParseGenreAnswer(string textFromUser)
        {
            var varName = Enum.TryParse(textFromUser, true, out Movie.Genre genre);
            return varName;
        }

        public int ParseIntegerAnswer(string textFromUser)
        {
            var success = int.TryParse(textFromUser, out var result);

            return result;
        }

        public bool ParseOpenAnswer(string textFromUser)
        {
            textFromUser = textFromUser.ToUpper();

            return string.IsNullOrWhiteSpace(textFromUser) != true;
        }

        public bool ParsePhoneNumberAnswer(string textFromUser)
        {
            var regexMobile = new Regex(@"^[0-9]{3}-[0-9]{3}-[0-9]{3}");
            var matchMobile = regexMobile.Match(textFromUser);

            var regexLandline = new Regex(@"^\+[0-9]{2} [0-9]{3}-[0-9]{2}-[0-9]{2}$");
            var matchLandline = regexLandline.Match(textFromUser);

            return matchMobile.Success || matchLandline.Success;
        }

        public bool ParseHourAnswer(string textFromUser)
        {
            var dateTimeFormatInfo = new DateTimeFormatInfo {ShortTimePattern = "HH:mm"};
            var resultDate = Convert.ToDateTime(textFromUser, dateTimeFormatInfo);
            var stringToBeChecked = resultDate.ToString("HH:mm");

            var regex = new Regex(@"^([0-1][0-9]|2[0-3]):[0-5][0-9]$");
            var match = regex.Match(stringToBeChecked);

            return match.Success;
        }

        public int ParseScreeningRoomNumber(string textFromUser)
        {
            var success = int.TryParse(textFromUser, out var result);

            if (result < 1) return 0;

            return result;
        }

        public bool ParseWebsiteAnswer(string textFromUser)
        {
            try
            {
                var request = WebRequest.Create(textFromUser) as HttpWebRequest;
                request.Method = "HEAD";

                var response = request.GetResponse() as HttpWebResponse;
                response.Close();

                return response.StatusCode == HttpStatusCode.OK;
            }
            catch
            {
                return false;
            }
        }

        public bool? ParseYesNoAnswer(string textFromUser)
        {
            textFromUser = textFromUser.ToUpper();

            if (textFromUser != "Y" && textFromUser != "N") return null;

            return textFromUser == "Y";
        }

        public DateTime GetStartDate(DateTime date)
        {
            const int counterMax = 7; //days of week
            var counter = 0;

            if (date.DayOfWeek == DayOfWeek.Friday)
                return date;

            do
            {
                date = date.AddDays(-1);

                if (date.DayOfWeek == DayOfWeek.Friday) return date;

                counter++;
            } while (counter < counterMax);

            return DateTime.MinValue;
        }
    }
}