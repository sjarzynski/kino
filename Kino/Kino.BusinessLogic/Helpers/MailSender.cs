﻿using System.Collections.Generic;
using System.Net.Mail;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.BusinessLogic.Helpers
{
    public class MailSender : IMailSender
    {
        public bool SendMail(List<TicketBl> tickets, double sumOfPrices)
        {
            var firstTicket = tickets[0];
            string singleMessage = "";
            string allMessages = "";
            string separator = "*********";

            try
            {
                //to make it work you need to use MailSlurper
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("localhost", 2500);

                mail.From = new MailAddress(firstTicket.Cinema.Email);
                mail.To.Add(firstTicket.Customer.Email);
                mail.Subject = "Cinema Ticket";

                string numberOfTickets = $"Number of tickets: {tickets.Count}";

                foreach (var singleTicket in tickets)
                {
                    singleMessage = $"Movie title: {singleTicket.Movie.Title}/ " +
                                    $"Date: {singleTicket.Date.ToShortDateString()}, {singleTicket.PlayingHour.Time.ToShortTimeString()}/ " +
                                    $"Cinema: {singleTicket.Cinema.Name}, {singleTicket.Cinema.Address}/ " +
                                    $"Row: {singleTicket.Row}, Seat: {singleTicket.Seat}";
                    allMessages = allMessages + separator + singleMessage;
                }

                string totalPrice = $"Total price: {sumOfPrices} PLN";

                mail.Body = numberOfTickets + separator + totalPrice + separator + allMessages;

                SmtpServer.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
