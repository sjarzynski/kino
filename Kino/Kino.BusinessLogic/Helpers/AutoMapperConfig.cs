﻿using AutoMapper;
using Kino.BusinessLogic.Models;
using Kino.Database.Models;

namespace Kino.BusinessLogic.Helpers
{
    public static class AutoMapperConfig
    {
        public static void CreateMap()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Movie, MovieBl>();
                cfg.CreateMap<MovieBl, Movie>();
                cfg.CreateMap<Cinema, CinemaBl>();
                cfg.CreateMap<CinemaBl, Cinema>();
                cfg.CreateMap<ScreeningRoom, ScreeningRoomBl>();
                cfg.CreateMap<ScreeningRoomBl, ScreeningRoom>();
                cfg.CreateMap<Programme, ProgrammeBl>();
                cfg.CreateMap<ProgrammeBl, Programme>();
                cfg.CreateMap<PlayingHoursScreeningRoom, PlayingHoursScreeningRoomBl>();
                cfg.CreateMap<PlayingHoursScreeningRoomBl, PlayingHoursScreeningRoom>();
                cfg.CreateMap<PlayingHour, PlayingHourBl>();
                cfg.CreateMap<PlayingHourBl, PlayingHour>();
                cfg.CreateMap<Customer, CustomerBl>();
                cfg.CreateMap<CustomerBl, Customer>();
                cfg.CreateMap<Ticket, TicketBl>();
                cfg.CreateMap<TicketBl, Ticket>();
                cfg.CreateMap<TicketBl, TicketBl>();
            });
        }
    }
}