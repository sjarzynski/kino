﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Kino.Database;
using Kino.Database.Models;

namespace Kino.BusinessLogic
{
    public class ScreeningRoomService : IScreeningRoomService
    {
        public async Task<int> AddScreeningRoomAsync(CinemaBl cinemaBl, int number, int rows, int seatsInRow)
        {
            var screeningRoom = new ScreeningRoom();

            using (var ctx = new CinemaContext())
            {
                var cinema = ctx.Cinemas.FirstOrDefault(c => c.Name == cinemaBl.Name);

                screeningRoom.Cinema = cinema;
                screeningRoom.Number = number;
                screeningRoom.Rows = rows;
                screeningRoom.SeatsInRow = seatsInRow;

                ctx.ScreeningRooms.Add(screeningRoom);
                await ctx.SaveChangesAsync();
            }

            return screeningRoom.Id;
        }

        public async Task<bool> CheckIfThisScreeningRoomExistInCinema(CinemaBl cinemaBl, int screeningRoomNumber)
        {
            ScreeningRoom screeningRoom;

            using (var ctx = new CinemaContext())
            {
                screeningRoom = await (ctx.ScreeningRooms
                    .Where(c => c.Cinema.Name == cinemaBl.Name))
                    .FirstOrDefaultAsync(sr => sr.Number == screeningRoomNumber);
            }

            return screeningRoom != null;
        }

        public async Task<bool> CheckIfCinemaHasAnyScreeningRoomAsync(CinemaBl cinemaBl)
        {
            List<ScreeningRoom> screeningRooms;

            using (var ctx = new CinemaContext())
            {
                screeningRooms = await ctx.ScreeningRooms.Include(x => x.Cinema).ToListAsync();
            }

            foreach (var scr in screeningRooms)
                if (scr.Cinema.Name == cinemaBl.Name)
                    return true;

            return false;
        }

        public async Task<List<ScreeningRoomBl>> GetAllScreeningRoomsInOneCinemaAsync(CinemaBl cinemaBl)
        {
            var screeningRoomsBl = new List<ScreeningRoomBl>();
            List<ScreeningRoom> screeningRooms;

            using (var ctx = new CinemaContext())
            {
                screeningRooms = await ctx.ScreeningRooms.ToListAsync();
            }

            foreach (var scr in screeningRooms)
                if (scr.CinemaId == cinemaBl.Id)
                    screeningRoomsBl.Add(Mapper.Map<ScreeningRoom, ScreeningRoomBl>(scr));

            return screeningRoomsBl;
        }

        public bool CheckIfScreeningRoomHasAllNecessaryProperties(ScreeningRoomBl sr)
        {
            if (sr.Number <= 0)
                return false;

            if (sr.Rows <= 0)
                return false;

            if (sr.SeatsInRow <= 0)
                return false;

            return true;
        }

        public bool CheckIfRowIsCorrect(int row, ScreeningRoomBl screeningRoom)
        {
            var maxRow = screeningRoom.Rows;
            var result = CheckIfNumberIsBetweenRange(row, maxRow);

            return result;
        }

        public bool CheckIfSeatIsCorrect(int seat, ScreeningRoomBl screeningRoom)
        {
            var maxSeat = screeningRoom.SeatsInRow;
            var result = CheckIfNumberIsBetweenRange(seat, maxSeat);

            return result;
        }

        private bool CheckIfNumberIsBetweenRange(int theNumber, int maxNumber)
        {
            if (theNumber > 0 && theNumber <= maxNumber)
            {
                return true;
            }

            return false;
        }
    }
}