﻿using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using System;
using System.Collections.Generic;

namespace Kino.BusinessLogic
{
    public class DiscountService : IDiscountService
    {
        public double ApplyDiscountToTenthTicket(double price, List<TicketBl> tickets)
        {
            int discountFrequency = 10;

            var ticketsInDatabaseNumber = tickets.Count;
            var currentTicket = ticketsInDatabaseNumber + 1;
            int modulo = currentTicket % discountFrequency;

            if (modulo != 0)
            {
                return price;
            }

            return 0;
        }

        public double ApplyMonthlyDiscount(double price, List<TicketBl> tickets)
        {
            var thisMonth = DateTime.Now.Month;
            var thisYear = DateTime.Now.Year;
            int nrOfTicketsThatStartsDiscount = 5;
            double discount = 0.80;

            var ticketsInTheMonth = new List<TicketBl>();
            foreach (var ticket in tickets)
            {
                if (ticket.Date.Month == thisMonth && ticket.Date.Year == thisYear)
                {
                    ticketsInTheMonth.Add(ticket);
                }
            }

            if (ticketsInTheMonth.Count < nrOfTicketsThatStartsDiscount)
            {
                return price;
            }

            return price * discount;
        }
    }
}
