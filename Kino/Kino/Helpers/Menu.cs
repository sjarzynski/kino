﻿using System;
using Kino.AppCli.Interfaces;

namespace Kino.AppCli.Helpers
{
    internal class Menu : IMenu
    {
        private readonly IPrinter _printer;
        private readonly IProgramViewModel _programViewModel;

        public Menu(IPrinter printer, IProgramViewModel programViewModel)
        {
            _printer = printer;
            _programViewModel = programViewModel;
        }

        public void DisplayMenu()
        {
            _printer.ColorAndPrintText("****************", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("Welcome to Kino!", ConsoleColor.Black, ConsoleColor.Yellow);
            _printer.ColorAndPrintText("****************", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("Choose an option:", ConsoleColor.Yellow);
            Console.WriteLine();
            _printer.ColorAndPrintText("1 - Add movie", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("2 - Add cinema", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("3 - Add screening room", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("4 - Add movie's programme", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("5 - Show cinema's programme", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("6 - Add customer", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("7 - Buy ticket", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("8 - Show customer's tickets", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("9 - Exit", ConsoleColor.Yellow);
            _printer.ColorAndPrintText("**************************", ConsoleColor.Yellow);
        }

        public bool TakeAction(int option)
        {
            switch (option)
            {
                case 1:
                    _programViewModel.AskForNewMovie();
                    return false;

                case 2:
                    _programViewModel.AskForNewCinema();
                    return false;

                case 3:
                    _programViewModel.AskForNewScreeningRoom();
                    return false;

                case 4:
                    _programViewModel.AskForNewProgramme();
                    return false;

                case 5:
                    _programViewModel.AskForPrintingProgramme();
                    return false;

                case 6:
                    _programViewModel.AskForNewCustomer();
                    return false;

                case 7:
                    _programViewModel.AddNewTicket();
                    return false;

                case 8:
                    _programViewModel.PrintCustomerTickets();
                    return false;

                case 9:
                    _printer.ColorAndPrintText("Thank you! Come back later!", ConsoleColor.Yellow);
                    return true;

                default:
                    _printer.ColorAndPrintText("Please select the right option!", ConsoleColor.Red);
                    return false;
            }
        }
    }
}