﻿using System;
using Kino.AppCli.Interfaces;
using Kino.BusinessLogic.Interfaces;

namespace Kino.AppCli.Helpers
{
    public class Checker : IChecker
    {
        private readonly ICinemaService _cinemaService;
        private readonly IMovieService _movieService;
        private readonly IPrinter _printer;

        public Checker(IMovieService movieService, IPrinter printer, ICinemaService cinemaService)
        {
            _movieService = movieService;
            _printer = printer;
            _cinemaService = cinemaService;
        }

        public bool CheckIfCinemaExists(string cinemaName)
        {
            var doesExist = _cinemaService.CheckIfCinemaExistsInDatabaseAsync(cinemaName).Result;

            return doesExist;
        }

        public bool CheckIfMovieExists(string movieTitle)
        {
            var doesExist = _movieService.CheckIfMovieExistsInDatabaseAsync(movieTitle).Result;
            if (!doesExist) return false;

            _printer.ColorAndPrintText(movieTitle.ToUpper() + " is already in database!", ConsoleColor.Red);
            return true;
        }

        public bool CheckIfScreeningRoomInCinemaExists(string cinemaName, int number)
        {
            var doesItExist = _cinemaService.CheckIfThereIsScreeningRoomInTheCinemaAsync(cinemaName, number).Result;

            return doesItExist;
        }
    }
}