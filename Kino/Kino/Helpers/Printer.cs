﻿using System;
using System.Globalization;
using System.Linq;
using Kino.AppCli.Interfaces;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppCli.Helpers
{
    internal class Printer : IPrinter
    {
        private readonly ICinemaService _cinemaService;
        private readonly IMovieService _movieService;
        private readonly IProgrammeService _programmeService;
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly ITicketService _ticketService;

        public Printer(IProgrammeService programmeService,
                        ICinemaService cinemaService,
                        IMovieService movieService,
                        IScreeningRoomService screeningRoomService,
                        ITicketService ticketService)
        {
            _programmeService = programmeService;
            _cinemaService = cinemaService;
            _movieService = movieService;
            _screeningRoomService = screeningRoomService;
            _ticketService = ticketService;
        }

        public void ColorAndPrintText(string txt, ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;

            Console.WriteLine(txt);
            Console.ResetColor();
        }

        public void PrintAllCinemas()
        {
            var cinemas = _cinemaService.GetAllCinemasAsync().Result.ToList();
            if (!cinemas.Any()) return;

            var lastCinema = cinemas.Last();

            Console.ForegroundColor = ConsoleColor.DarkYellow;

            foreach (var movie in cinemas)
                if (lastCinema.Name == movie.Name)
                    Console.Write(movie.Name);
                else
                    Console.Write(movie.Name + ", ");

            Console.Write(Environment.NewLine);
            Console.ResetColor();
        }

        public void PrintAllGenres()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            var lastGenre = Enum.GetValues(typeof(MovieBl.Genre)).Cast<MovieBl.Genre>().Max();

            foreach (var genre in Enum.GetNames(typeof(MovieBl.Genre)))
                if (lastGenre.ToString() == genre)
                    Console.Write(genre);
                else
                    Console.Write(genre + ", ");
            Console.Write(Environment.NewLine);
            Console.ResetColor();
        }

        public void PrintAllTicketsOfSingleCustomer(string customerEmail)
        {
            int ticketNumber = 1;
            var tickets = _ticketService.GetAllTicketsOfSingleCustomerAsync(customerEmail).Result;
            if (!tickets.Any()) return;

            var lastTicket = tickets.Last();

            Console.ForegroundColor = ConsoleColor.DarkYellow;

            foreach (var ticket in tickets)
            {       
                Console.WriteLine($"*** Ticket nr {ticketNumber} ***");
                Console.WriteLine($"Movie title: {ticket.Movie.Title.ToUpper()}");
                Console.WriteLine($"Movie show date: {ticket.Date.ToShortDateString()}");
                Console.WriteLine($"Movie show time: {ticket.PlayingHour.Time.ToShortTimeString()}");
                Console.WriteLine($"Cinema name: {ticket.Cinema.Name}");
                Console.WriteLine($"Cinema address: {ticket.Cinema.Address}");
                Console.WriteLine($"Row number: {ticket.Row}");
                Console.WriteLine($"Seat number: {ticket.Seat}");
                Console.WriteLine("*********************************");

                ticketNumber++;
            }
            Console.ResetColor();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        public void PrintAllMovies()
        {
            var movies = _movieService.GetAllMoviesAsync().Result.ToList();
            if (!movies.Any()) return;

            var lastMovie = movies.Last();

            Console.ForegroundColor = ConsoleColor.DarkYellow;

            foreach (var movie in movies)
                if (lastMovie.Title == movie.Title)
                    Console.Write(movie.Title);
                else
                    Console.Write(movie.Title + ", ");

            Console.Write(Environment.NewLine);
            Console.ResetColor();
        }

        public void PrintAllScreeningRoomsForOneCinema(CinemaBl cinema)
        {
            var screeningRooms = _screeningRoomService.GetAllScreeningRoomsInOneCinemaAsync(cinema).Result;
            if (screeningRooms.Count == 0) return;

            var lastScreeningRoom = screeningRooms.Last();

            Console.ForegroundColor = ConsoleColor.DarkYellow;

            Console.Write("Existing screening rooms: ");

            foreach (var screeningRoom in screeningRooms)
                if (lastScreeningRoom.Number == screeningRoom.Number)
                    Console.Write(Convert.ToInt32(screeningRoom.Number));
                else
                    Console.Write(Convert.ToInt32(screeningRoom.Number) + ", ");

            Console.Write(Environment.NewLine);
            Console.ResetColor();
        }

        public bool PrintProgrammesForCinema(CinemaBl cinema, DateTime startDate)
        {
            var programmes = _programmeService.GetProgrammesForCinemaAsync(startDate, cinema).Result;
            if (programmes == null) return false;

            if (programmes.Count == 0) return false;

            var dateRange = _programmeService.GetDateRange(startDate);

            ColorAndPrintText(cinema.Name.ToUpper(), ConsoleColor.Black, ConsoleColor.Yellow);
            ColorAndPrintText("****************", ConsoleColor.Yellow);
            ColorAndPrintText(dateRange, ConsoleColor.Yellow);
            ColorAndPrintText("****************", ConsoleColor.Yellow);

            foreach (var programme in programmes)
            {
                if (programme.StartDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) !=
                    startDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)) continue;

                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Title: " + programme.Movie.Title.ToUpper());
                Console.WriteLine("Genre: " + programme.Movie.MovieGenre);
                Console.WriteLine("Runtime: " + programme.Movie.Runtime + " minutes");
                foreach (var phsr in programme.PlayingHoursScreeningRooms)
                {
                    Console.Write("Screening Room " + phsr.ScreeningRoom.Number + ": ");
                    if (phsr.PlayingHours.Count == 0)
                    {
                        continue;
                    }
                    var lastPlayingHour = phsr.PlayingHours.Last();

                    foreach (var playingHour in phsr.PlayingHours)
                        if (playingHour.Time == lastPlayingHour.Time)
                            Console.Write(playingHour.Time.ToString("HH:mm"));
                        else
                            Console.Write(playingHour.Time.ToString("HH:mm") + ", ");
                    Console.Write(Environment.NewLine);
                }

                Console.ResetColor();
                ColorAndPrintText("****************", ConsoleColor.Yellow);
            }

            return true;
        }
    }
}