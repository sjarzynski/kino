﻿using System;
using Kino.AppCli.Interfaces;
using Kino.BusinessLogic.Interfaces;

namespace Kino.AppCli
{
    public class Starter : IStarter
    {
        private readonly IMenu _menu;
        private readonly IParser _parser;

        public Starter(IParser parser, IMenu menu)
        {
            _parser = parser;
            _menu = menu;
        }

        public void StartApp()
        {
            int result;
            bool closeApp;

            do
            {
                _menu.DisplayMenu();

                var option = Console.ReadLine();
                result = _parser.ParseChosenOption(option);
                closeApp = _menu.TakeAction(result);

                Console.WriteLine();
            } while (!closeApp);

            Console.ReadLine();
        }
    }
}