﻿namespace Kino.AppCli.Interfaces
{
    public interface IStarter
    {
        void StartApp();
    }
}