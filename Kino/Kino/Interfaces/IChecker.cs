﻿namespace Kino.AppCli.Interfaces
{
    public interface IChecker
    {
        bool CheckIfCinemaExists(string cinemaName);
        bool CheckIfMovieExists(string movieTitle);
        bool CheckIfScreeningRoomInCinemaExists(string cinemaName, int number);
    }
}