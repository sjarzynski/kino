﻿namespace Kino.AppCli.Interfaces
{
    public interface IMenu
    {
        void DisplayMenu();
        bool TakeAction(int option);
    }
}