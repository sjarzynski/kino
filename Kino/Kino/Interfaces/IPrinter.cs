﻿using System;
using Kino.BusinessLogic.Models;

namespace Kino.AppCli.Interfaces
{
    public interface IPrinter
    {
        void ColorAndPrintText(string txt, ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black);
        void PrintAllCinemas();
        void PrintAllGenres();
        void PrintAllMovies();
        void PrintAllScreeningRoomsForOneCinema(CinemaBl cinema);
        bool PrintProgrammesForCinema(CinemaBl cinema, DateTime startDate);
        void PrintAllTicketsOfSingleCustomer(string customerEmail);
    }
}