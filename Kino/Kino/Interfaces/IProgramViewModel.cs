﻿using Kino.BusinessLogic.Models;

namespace Kino.AppCli.Interfaces
{
    public interface IProgramViewModel
    {
        int AskForIntegerAnswer(string question);
        void AskForNewCinema();
        void AskForNewMovie();
        void AskForNewProgramme();
        void AskForNewScreeningRoom();
        string AskForOpenAnswer(string question);
        void AskForPrintingProgramme();
        string AskForSelectingCinema(string question);
        string AskForSelectingMovie(string question);
        int AskForSelectingScreeningRoom(string question, CinemaBl cinema);
        bool AskForYesNoAnswer(string question);
        void AskForNewCustomer();
        void AddNewTicket();
        void PrintCustomerTickets();
    }
}