﻿using System;
using System.Collections.Generic;
using System.IO;
using Kino.AppCli.Interfaces;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppCli
{
    internal class ProgramViewModel : IProgramViewModel
    {
        private readonly IChecker _checker;
        private readonly ICinemaService _cinemaService;
        private readonly IMovieService _movieService;
        private readonly IParser _parser;
        private readonly IPrinter _printer;
        private readonly IProgrammeService _programmeService;
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly ICustomerService _customerService;
        private readonly ITicketService _ticketService;
        private readonly IMailSender _mailSender;

        public ProgramViewModel(IMovieService movieService,
                                ICinemaService cinemaService,
                                IParser parser,
                                IPrinter printer,
                                IChecker checker,
                                IScreeningRoomService screeningRoomService,
                                IProgrammeService programmeService,
                                ICustomerService customerService,
                                ITicketService ticketService,
                                IMailSender mailSender)
        {
            _movieService = movieService;
            _cinemaService = cinemaService;
            _parser = parser;
            _printer = printer;
            _checker = checker;
            _screeningRoomService = screeningRoomService;
            _programmeService = programmeService;
            _customerService = customerService;
            _ticketService = ticketService;
            _mailSender = mailSender;
        }

        public void PrintCustomerTickets()
        {
            var customerBl = new CustomerBl();
            customerBl.Email = AskForEmailAnswer("Type customer's e-mail address");
            customerBl = _customerService.GetCustomerAsync(customerBl).Result;
            if (customerBl == null)
            {
                _printer.ColorAndPrintText("There is no such customer in database!", ConsoleColor.Red);
                return;
            }

            _printer.PrintAllTicketsOfSingleCustomer(customerBl.Email);
        }

        public void AddNewTicket()
        {
            var customerBl = new CustomerBl();
            customerBl.Email = AskForEmailAnswer("Type customer's e-mail address");
            customerBl = _customerService.GetCustomerAsync(customerBl).Result;
            if (customerBl == null)
            {
                _printer.ColorAndPrintText("There is no such customer in database!", ConsoleColor.Red);
                return;
            }

            TicketBl ticketBl = new TicketBl();
            ticketBl.Customer = customerBl;

            var cinemaName = AskForSelectingCinema("Select the cinema");
            ticketBl.Cinema = _cinemaService.GetCinemaAsync(cinemaName).Result;
            if (ticketBl.Cinema == null)
            {
                _printer.ColorAndPrintText(cinemaName.ToUpper() + " cinema doesn't exist!", ConsoleColor.Red);
                return;
            }

            DateTime todayDate = AskForDateAnswer("Type the day you want to buy ticket for (mm/dd/yyyy)");
            ticketBl.Date = todayDate;
            DateTime startDate = _programmeService.SetCurrentStartDate(ticketBl.Date);

            var isPrinted = _printer.PrintProgrammesForCinema(ticketBl.Cinema, startDate);
            if (isPrinted == false)
            {
                var dateRange = _programmeService.GetDateRange(startDate);
                _printer.ColorAndPrintText(
                    cinemaName.ToUpper() + " cinema doesn't have any programme for " + dateRange + " week!",
                    ConsoleColor.Red);
                return;
            }

            List<ProgrammeBl> programmesBl = _programmeService.GetProgrammesForCinemaAsync(startDate, ticketBl.Cinema).Result;

            _printer.ColorAndPrintText("Type movie, screening room and playing hour (movie/number/xx:xx)", ConsoleColor.Yellow);
            string movieShow = Console.ReadLine();

            ticketBl = _programmeService.GetMovieScreeningRoomAndPlayingHour(movieShow, programmesBl, ticketBl);
            if (ticketBl.Movie == null)
            {
                _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
                return;
            }

            var doYouWantToAddAnotherTicket = false;
            var tickets = new List<TicketBl>();
            double sumOfPrices = ticketBl.Price;
            do
            {
                var row = AskForIntegerAnswer("Type row in screening room (from 1 to " + ticketBl.ScreeningRoom.Rows +
                                              ")");
                bool isRowCorrect = _screeningRoomService.CheckIfRowIsCorrect(row, ticketBl.ScreeningRoom);
                if (!isRowCorrect)
                {
                    _printer.ColorAndPrintText("Incorrect row!", ConsoleColor.Red);
                    return;
                }

                ticketBl.Row = row;

                var seat = AskForIntegerAnswer("Type seat in the screening room (from 1 to " +
                                               ticketBl.ScreeningRoom.SeatsInRow + ")");
                bool isSeatCorrect = _screeningRoomService.CheckIfSeatIsCorrect(seat, ticketBl.ScreeningRoom);
                if (!isSeatCorrect)
                {
                    _printer.ColorAndPrintText("Incorrect seat!", ConsoleColor.Red);
                    return;
                }

                ticketBl.Seat = seat;

                bool doesTicketExist = _ticketService.CheckIfTicketExistAsync(ticketBl).Result;
                if (doesTicketExist)
                {
                    _printer.ColorAndPrintText("This seat is already taken!", ConsoleColor.Red);
                    return;
                }

                ticketBl.Price = _ticketService.GetTicketPrice(ticketBl);

                var ticketId = _ticketService.AddTicketAsync(ticketBl).Result;
                _printer.ColorAndPrintText("Ticket added! Ticket ID: " + ticketId, ConsoleColor.Green);

                var duplicatedTicket = _ticketService.DuplicateTicket(ticketBl);
                tickets.Add(duplicatedTicket);
                sumOfPrices = _ticketService.SumTicketsPrice(sumOfPrices, duplicatedTicket.Price);

                doYouWantToAddAnotherTicket = AskForYesNoAnswer("Would you like to add another ticket? (Y/N)");            
            } while (doYouWantToAddAnotherTicket);

            var isMailSent = _mailSender.SendMail(tickets, sumOfPrices);
            if (isMailSent)
            {
                _printer.ColorAndPrintText("Mail sent to: " + ticketBl.Customer.Email + "!", ConsoleColor.Green);
            }
        }

        public void AskForNewCustomer()
        {
            var doYouWantToAddAnotherCustomer = false;

            do
            {
                var customerBl = new CustomerBl();
                customerBl.Email = AskForEmailAnswer("Type customer's e-mail address");
                var doesCustomerExist = _customerService.CheckIfCustomerExistsInDatabaseAsync(customerBl).Result;
                if (doesCustomerExist)
                {
                    _printer.ColorAndPrintText("This e-mail already exists in customers database!", ConsoleColor.Red);
                    return;
                }

                customerBl.FirstName = AskForOpenAnswer("Type customer's first name");
                customerBl.LastName = AskForOpenAnswer("Type customer's last name");

                _customerService.AddCustomerAsync(customerBl).Wait();
                _printer.ColorAndPrintText(customerBl.FirstName.ToUpper() + " added to database!", ConsoleColor.Green);

                doYouWantToAddAnotherCustomer = AskForYesNoAnswer("Would you like to add another customer? (Y/N)");
            } while (doYouWantToAddAnotherCustomer);
        }

        public int AskForIntegerAnswer(string question)
        {
            int result;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();
                result = _parser.ParseIntegerAnswer(userAnswer);
                if (result == 0) _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (result == 0);

            return result;
        }

        public void AskForNewCinema()
        {
            var doYouWantToAddAnotherCinema = false;

            do
            {
                var name = AskForOpenAnswer("Type name of the cinema");
                if (_checker.CheckIfCinemaExists(name))
                {
                    _printer.ColorAndPrintText(name.ToUpper() + " is already in database!", ConsoleColor.Red);
                    continue;
                }

                var address = AskForOpenAnswer("Type address of the cinema");
                var eMail = AskForEmailAnswer("Type cinema's e-mail address");
                var phoneNumber =
                    AskForPhoneNumberAnswer(
                        "Type cinema's phone number - mobile (XXX-XXX-XXX) or landline (+XX XXX-XX-XX)");
                var openHours = AskForHoursAnswer("Type cinema's opening hours (hh:mm)");
                var closeHours = AskForHoursAnswer("Type cinema's closing hours (hh:mm)");

                _cinemaService.AddCinemaAsync(name, address, eMail, phoneNumber, openHours, closeHours).Wait();
                _printer.ColorAndPrintText(name + " added to database!", ConsoleColor.Green);

                doYouWantToAddAnotherCinema = AskForYesNoAnswer("Would you like to add another cinema? (Y/N)");
            } while (doYouWantToAddAnotherCinema);
        }

        public void AskForNewMovie()
        {
            var doYouWantToAddAnotherMovie = false;

            do
            {
                var title = AskForOpenAnswer("Type name of the movie");
                if (_checker.CheckIfMovieExists(title)) continue;

                var description = AskForOpenAnswer("Type short description of the movie");
                var genre = AskForGenreAnswer("Type genre of the movie");
                var runtime = AskForIntegerAnswer("Type runtime of the movie (minutes)");
                var webUrl = AskForWebsiteAnswer("Type full Filmweb URL to the movie");

                _movieService.AddMovieAsync(title, description, genre, runtime, webUrl).Wait();
                _printer.ColorAndPrintText(title + " added to database!", ConsoleColor.Green);

                doYouWantToAddAnotherMovie = AskForYesNoAnswer("Would you like to add another movie? (Y/N)");
            } while (doYouWantToAddAnotherMovie);
        }

        public void AskForNewProgramme()
        {
            var programme = new ProgrammeBl();

            var cinema = ChooseCinema();
            var doesScreeningRoomForTheCinemaExist =
                _screeningRoomService.CheckIfCinemaHasAnyScreeningRoomAsync(cinema).Result;
            if (!doesScreeningRoomForTheCinemaExist)
            {
                _printer.ColorAndPrintText(cinema.Name.ToUpper() + " cinema doesn't have any screening rooms!",
                    ConsoleColor.Red);
                return;
            }

            programme.Cinema = cinema;

            var movie = ChooseMovie();
            programme.Movie = movie;

            var startDate = ChooseStartDate(cinema, movie);
            programme.StartDate = startDate;

            var dateRange = _programmeService.GetDateRange(startDate);

            bool doYouWantToAddPlayingHoursForAnotherScreeningRoom;

            do
            {
                var screeningRoomPlayingHours = new PlayingHoursScreeningRoomBl();
                programme.PlayingHoursScreeningRooms.Add(screeningRoomPlayingHours);

                var screeningRoom = ChooseScreeningRoom(cinema, dateRange);
                if (screeningRoom == null)
                {
                    _printer.ColorAndPrintText(
                        "There is no such screening room in " + cinema.Name.ToUpper() + " cinema!", ConsoleColor.Red);
                    return;
                }

                screeningRoomPlayingHours.ScreeningRoom = screeningRoom;
                screeningRoomPlayingHours.PlayingHours =
                    ChoosePlayingHoursForTheScreeningRoom(screeningRoomPlayingHours, screeningRoom, programme);

                doYouWantToAddPlayingHoursForAnotherScreeningRoom =
                    AskForYesNoAnswer("Would you like to add playing hours for another screening room? (Y/N)");
            } while (doYouWantToAddPlayingHoursForAnotherScreeningRoom);

            _programmeService.AddProgrammeAsync(programme);
            _printer.ColorAndPrintText("Programme successfully added!", ConsoleColor.Green);
        }

        public void AskForNewScreeningRoom()
        {
            var doYouWantToAddAnotherScreeningRoom = false;

            do
            {
                var cinemaName = AskForSelectingCinema("Select the cinema");
                var cinema = _cinemaService.GetCinemaAsync(cinemaName).Result;
                if (cinema == null)
                {
                    _printer.ColorAndPrintText(
                        "Can't add a screening room, because " + cinemaName.ToUpper() + " cinema doesn't exist!",
                        ConsoleColor.Red);
                    continue;
                }

                var number = AskForSelectingScreeningRoom("Type number of the NEW screening room (>= 1)", cinema);
                if (_checker.CheckIfScreeningRoomInCinemaExists(cinemaName, number))
                {
                    _printer.ColorAndPrintText(
                        "Screening room " + number + " already exists in cinema " + cinemaName.ToUpper() + "!",
                        ConsoleColor.Red);
                    continue;
                }

                var rows = AskForIntegerAnswer("Type number of rows in the screening room");
                var seatsInRow = AskForIntegerAnswer("Type number of seats in a row");

                _screeningRoomService.AddScreeningRoomAsync(cinema, number, rows, seatsInRow).Wait();
                _printer.ColorAndPrintText("Screening room added to database!", ConsoleColor.Green);

                doYouWantToAddAnotherScreeningRoom =
                    AskForYesNoAnswer("Would you like to add another screening room? (Y/N)");
            } while (doYouWantToAddAnotherScreeningRoom);
        }

        public string AskForOpenAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseOpenAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (true);
        }

        public void AskForPrintingProgramme()
        {
            DateTime startDate;

            var cinemaName = AskForSelectingCinema("Select the cinema");
            var cinema = _cinemaService.GetCinemaAsync(cinemaName).Result;
            if (cinema == null)
            {
                _printer.ColorAndPrintText(cinemaName.ToUpper() + " cinema doesn't exist!", ConsoleColor.Red);
                return;
            }

            var doYouWantCurrentWeek = AskForYesNoAnswer("Would you like see the programme for current week? (Y/N)");
            if (doYouWantCurrentWeek)
            {
                var currentStartDate = _programmeService.SetCurrentStartDate();
                var isPrinted = _printer.PrintProgrammesForCinema(cinema, currentStartDate);
                if (isPrinted == false)
                {
                    var dateRange = _programmeService.GetDateRange(currentStartDate);
                    _printer.ColorAndPrintText(
                        cinemaName.ToUpper() + " cinema doesn't have any programme for " + dateRange + " week!",
                        ConsoleColor.Red);
                    return;
                }

                startDate = currentStartDate;
            }
            else
            {
                var startDateForPrinting =
                    AskForStartDateAnswer("Type starting day (Friday) in this format: mm/dd/yyyy");
                var isPrinted = _printer.PrintProgrammesForCinema(cinema, startDateForPrinting);
                if (isPrinted == false)
                {
                    var dateRange = _programmeService.GetDateRange(startDateForPrinting);
                    _printer.ColorAndPrintText(
                        cinemaName.ToUpper() + " cinema doesn't have any programme for " + dateRange + " week!",
                        ConsoleColor.Red);
                    return;
                }

                startDate = startDateForPrinting;
            }

            var doYouWantToPrintIt = AskForYesNoAnswer("Would you like to save the programme to a file? (Y/N)");
            if (doYouWantToPrintIt)
            {
                var fileExtension = AskForFileExtensionAnswer("Select JSON or XML");
                var programmes = _programmeService.GetProgrammesForCinemaAsync(startDate, cinema).Result;
                var currentDirectory = Directory.GetCurrentDirectory();

                if (fileExtension == "JSON")
                {
                    _programmeService.PrintToJson(programmes);
                    _printer.ColorAndPrintText("File saved as " + currentDirectory + "\\programme.json",
                        ConsoleColor.Green);
                }
                else
                {
                    _programmeService.PrintToXml(programmes);
                    _printer.ColorAndPrintText("File saved as " + currentDirectory + "\\programme.xml",
                        ConsoleColor.Green);
                }
            }
        }

        public string AskForSelectingCinema(string question)
        {
            string userAnswer;
            bool isCorrect;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                _printer.PrintAllCinemas();
                userAnswer = Console.ReadLine();

                isCorrect = _parser.ParseOpenAnswer(userAnswer);
                if (!isCorrect)
                    _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (!isCorrect);

            return userAnswer;
        }

        public string AskForSelectingMovie(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                _printer.PrintAllMovies();
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseOpenAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (true);
        }

        public int AskForSelectingScreeningRoom(string question, CinemaBl cinema)
        {
            int result;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                _printer.PrintAllScreeningRoomsForOneCinema(cinema);
                var userAnswer = Console.ReadLine();
                result = _parser.ParseIntegerAnswer(userAnswer);
                if (result == 0) _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (result == 0);

            return result;
        }

        public bool AskForYesNoAnswer(string question)
        {
            bool isCorrect;
            var result = false;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var parsedUserAnswer = _parser.ParseYesNoAnswer(userAnswer);
                if (parsedUserAnswer == null)
                {
                    _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
                    isCorrect = false;
                }
                else
                {
                    result = (bool) parsedUserAnswer;
                    isCorrect = true;
                }
            } while (isCorrect == false);

            return result;
        }

        private string AskForEmailAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseEmailAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("This is not a correct e-mail address!", ConsoleColor.Red);
            } while (true);
        }

        private string AskForFileExtensionAnswer(string question)
        {
            bool isCorrect;
            var result = "";

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var parsedUserAnswer = _parser.ParseFileExtensionAnswer(userAnswer);
                if (parsedUserAnswer == false)
                {
                    _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
                    isCorrect = false;
                }
                else
                {
                    if (userAnswer != null) result = userAnswer.ToUpper();
                    isCorrect = true;
                }
            } while (isCorrect == false);

            return result;
        }

        private string AskForGenreAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                _printer.PrintAllGenres();
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseGenreAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("Wrong answer!", ConsoleColor.Red);
            } while (true);
        }

        private DateTime AskForHoursAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseHourAnswer(userAnswer);
                if (isCorrect)
                {
                    var hour = _parser.ConvertStringToHoursAndMinutes(userAnswer);
                    return hour;
                }

                _printer.ColorAndPrintText("This is not a correct hour!", ConsoleColor.Red);
            } while (true);
        }

        private string AskForPhoneNumberAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParsePhoneNumberAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("Type XXX-XXX-XXX for mobile or +XX XXX-XX-XX for landline!",
                    ConsoleColor.Red);
            } while (true);
        }

        private DateTime AskForPlayingHourAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseHourAnswer(userAnswer);
                if (isCorrect)
                {
                    var hour = _parser.ConvertStringToHoursAndMinutes(userAnswer);
                    return hour;
                }

                _printer.ColorAndPrintText("This is not a correct hour!", ConsoleColor.Red);
            } while (true);
        }

        private DateTime AskForDateAnswer(string question)
        {
            string userAnswer;
            bool isCorrectDate;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                userAnswer = Console.ReadLine();

                isCorrectDate = _parser.ParseDateAnswer(userAnswer);
                if (!isCorrectDate)
                {
                    _printer.ColorAndPrintText("This is not a correct date!", ConsoleColor.Red);
                    continue;
                }

            } while (isCorrectDate == false);

            var date = _parser.GetDateFromString(userAnswer);
            return date;
        }

        private DateTime AskForStartDateAnswer(string question)
        {
            string userAnswer;
            bool isCorrectDate;
            var isFriday = false;

            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                userAnswer = Console.ReadLine();

                isCorrectDate = _parser.ParseDateAnswer(userAnswer);
                if (!isCorrectDate)
                {
                    _printer.ColorAndPrintText("This is not a correct date!", ConsoleColor.Red);
                    continue;
                }

                isFriday = _parser.CheckIfDateIsFriday(userAnswer);
                if (!isFriday) _printer.ColorAndPrintText("This is not a Friday!", ConsoleColor.Red);
            } while (isCorrectDate == false || isFriday == false);

            var week = _parser.GetDateFromString(userAnswer);
            return week;
        }

        private string AskForWebsiteAnswer(string question)
        {
            do
            {
                _printer.ColorAndPrintText(question, ConsoleColor.Yellow);
                var userAnswer = Console.ReadLine();

                var isCorrect = _parser.ParseWebsiteAnswer(userAnswer);
                if (isCorrect) return userAnswer;

                _printer.ColorAndPrintText("The website does not exist!", ConsoleColor.Red);
            } while (true);
        }

        private CinemaBl ChooseCinema()
        {
            CinemaBl cinema;

            do
            {
                var name = AskForSelectingCinema("Select the cinema");
                cinema = _cinemaService.GetCinemaAsync(name).Result;

                if (cinema == null)
                    _printer.ColorAndPrintText("There is no " + name.ToUpper() + " cinema in database!",
                        ConsoleColor.Red);
            } while (cinema == null);

            return cinema;
        }

        private MovieBl ChooseMovie()
        {
            MovieBl movie;

            do
            {
                var title = AskForSelectingMovie("Select the movie");
                movie = _movieService.GetMovieAsync(title).Result;

                if (movie == null)
                    _printer.ColorAndPrintText("There is no " + title.ToUpper() + " movie in database!",
                        ConsoleColor.Red);
            } while (movie == null);

            return movie;
        }

        private List<PlayingHourBl> ChoosePlayingHoursForTheScreeningRoom(
            PlayingHoursScreeningRoomBl playingHoursScreeningRoom, ScreeningRoomBl screeningRoom,
            ProgrammeBl programme)
        {
            var doYouWantToAddAnotherPlayingHour = false;

            do
            {
                var time = AskForPlayingHourAnswer("Type playing hour (hh:mm) for the movie in screening room number " +
                                                   screeningRoom.Number);
                var playingHour = new PlayingHourBl {Time = time};
                var isHourTooEarly = _programmeService.CheckIfHourIsTooEarlyOrTooLate(programme.Cinema, playingHour);
                if (isHourTooEarly)
                {
                    _printer.ColorAndPrintText(
                        "Movie cannot be played before opening hours or after closing hours of the cinema!",
                        ConsoleColor.Red);
                    continue;
                }

                var isHourOccupied = _programmeService.CheckIfHourIsOccupied(programme, screeningRoom, playingHour);
                if (isHourOccupied)
                {
                    _printer.ColorAndPrintText("These playing hours in this screening room are already occupied!",
                        ConsoleColor.Red);
                    continue;
                }

                playingHoursScreeningRoom.PlayingHours.Add(playingHour);

                doYouWantToAddAnotherPlayingHour =
                    AskForYesNoAnswer("Would you like to add another playing hour in this screening room? (Y/N)");
            } while (doYouWantToAddAnotherPlayingHour);

            return playingHoursScreeningRoom.PlayingHours;
        }

        private ScreeningRoomBl ChooseScreeningRoom(CinemaBl cinema, string dateRange)
        {
            _printer.ColorAndPrintText("Programme for " + dateRange + " week", ConsoleColor.Yellow);
            var number =
                AskForSelectingScreeningRoom("Select the screening room in cinema " + cinema.Name.ToUpper(), cinema);
            var screeningRoom = _cinemaService.GetScreeningRoomForTheCinemaAsync(cinema, number).Result;

            return screeningRoom;
        }

        private DateTime ChooseStartDate(CinemaBl cinema, MovieBl movie)
        {
            var lastStartDateInDatabase = _programmeService.GetLastStartDateAsync(cinema, movie).Result;
            var isDateEmpty = _programmeService.CheckIfDateIsEmpty(lastStartDateInDatabase);

            if (isDateEmpty)
            {
                var newStartDate = AskForStartDateAnswer("Type starting day (Friday) for the programme (mm/dd/yyyy)");
                return newStartDate;
            }

            var nextStartDate = _programmeService.GetNextStartDate(lastStartDateInDatabase);
            return nextStartDate;
        }
    }
}