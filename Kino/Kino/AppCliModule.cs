﻿using System.Collections.Generic;
using Kino.AppCli.Helpers;
using Kino.AppCli.Interfaces;
using Kino.BusinessLogic;
using Ninject.Modules;

namespace Kino.AppCli
{
    public class AppCliModule : NinjectModule
    {
        public override void Load()
        {
            if (Kernel == null) return;

            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Bind<IPrinter>().To<Printer>();
            Kernel.Bind<IProgramViewModel>().To<ProgramViewModel>();
            Kernel.Bind<IStarter>().To<Starter>();
            Kernel.Bind<IChecker>().To<Checker>();

            Kernel.Load(new List<NinjectModule>
            {
                new BusinessLogicModule()
            });
        }
    }
}