﻿using System;
using Kino.BusinessLogic.Helpers;
using Ninject;

namespace Kino.AppCli
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "Kino, ver. 1.00";

            AutoMapperConfig.CreateMap();
            IKernel kernel = new StandardKernel(new AppCliModule());
            var run = kernel.Get<Starter>();

            run.StartApp();
        }
    }
}