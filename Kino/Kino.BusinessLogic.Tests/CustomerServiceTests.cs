﻿using NUnit.Framework;
using Kino.BusinessLogic.Models;
using Kino.BusinessLogic.Interfaces;
using Moq;

namespace Kino.BusinessLogic.Tests
{
    [TestFixture]
    public class CustomerServiceTests
    {
        [Test]
        public void CheckIfCustomerHasAllNecessaryProperties_AllPropertiesProvided_TrueReturned()
        {
            //Arrange
            CustomerBl customer = new CustomerBl
            {
                FirstName = "Michael",
                LastName = "Jackson",
                Email = "michael.jackson@gmail.com",
            };

            var mockParser = new Mock<IParser>();
            mockParser.Setup(x => x.ParseEmailAnswer(customer.Email)).Returns(true);

            var customerService = new CustomerService(mockParser.Object);

            //Act
            var result = customerService.CheckIfCustomerHasAllNecessaryProperties(customer);

            //Assert
            Assert.True(result);
        }
       
        [Test]
        public void CheckIfCustomerHasAllNecessaryProperties_MissingRequiredProperties_FalseReturned()
        {
            //Arrange
            CustomerBl customer = new CustomerBl
            {
                FirstName = "",
                LastName = "Bryant",
                Email = "kobe.bryant@gmail.com",
            };

            var mockParser = new Mock<IParser>();
            mockParser.Setup(x => x.ParseEmailAnswer(customer.Email)).Returns(true);

            var customerService = new CustomerService(mockParser.Object);

            //Act
            var result = customerService.CheckIfCustomerHasAllNecessaryProperties(customer);

            //Assert
            Assert.False(result);
        }
    }
}
