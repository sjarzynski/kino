﻿using System;
using System.Collections.Generic;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;
using Moq;
using NUnit.Framework;

namespace Kino.BusinessLogic.Tests
{
    [TestFixture]
    public class ProgrammeServiceTests
    {
        [Test]
        public void CheckIfDateIsEmpty_ValidInput_ValidOutput()
        {
            //Arrange
            var date = new DateTime(2019, 04, 16, 18, 00, 00);
            var programmeService = new ProgrammeService(null, null);

            //Act
            var result = programmeService.CheckIfDateIsEmpty(date);

            //Assert
            Assert.False(result);
        }

        [Test]
        public void CheckIfHourIsTooEarlyOrTooLate_LatePlayingHour_InconsistencyDetected()
        {
            //Arrange
            var cinema = new CinemaBl
            {
                OpenHours = new DateTime(2019, 04, 21, 08, 00, 00),
                CloseHours = new DateTime(2019, 04, 21, 20, 00, 00)
            };

            var playingHour = new PlayingHourBl {Time = new DateTime(2019, 04, 21, 22, 00, 00)};
            var programmeService = new ProgrammeService(null, null);

            //Act
            var result = programmeService.CheckIfHourIsTooEarlyOrTooLate(cinema, playingHour);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void CheckIfHourIsTooEarlyOrTooLate_ValidInput_ValidOutput()
        {
            //Arrange
            var cinema = new CinemaBl
            {
                OpenHours = new DateTime(2019, 04, 21, 08, 00, 00),
                CloseHours = new DateTime(2019, 04, 21, 20, 00, 00)
            };

            var playingHour = new PlayingHourBl {Time = new DateTime(2019, 04, 21, 16, 00, 00)};
            var programmeService = new ProgrammeService(null, null);

            //Act
            var result = programmeService.CheckIfHourIsTooEarlyOrTooLate(cinema, playingHour);

            //Assert
            Assert.False(result);
        }

        [Test]
        public void CheckIfProgrammeHasAllNecessaryProperties_ValidInput_ValidOutput()
        {
            //Arrange
            var programme = new ProgrammeBl
            {
                Cinema = new CinemaBl {Name = "Multikino"},
                Movie = new MovieBl {Title = "Lord of the Rings"},
                StartDate = DateTime.Now
            };
            var playingHoursScreeningRooms = new List<PlayingHoursScreeningRoomBl>();
            var pHsr1 = new PlayingHoursScreeningRoomBl {ScreeningRoom = new ScreeningRoomBl {Number = 2}};
            var playingHour = pHsr1.PlayingHours;
            var ph1 = new PlayingHourBl {Time = DateTime.Now};
            var ph2 = new PlayingHourBl {Time = DateTime.Now};
            playingHour.Add(ph1);
            playingHour.Add(ph2);

            playingHoursScreeningRooms.Add(pHsr1);
            programme.PlayingHoursScreeningRooms = playingHoursScreeningRooms;

            var mockParser = new Mock<IParser>();
            mockParser.Setup(x => x.ParseDateAnswer(programme.StartDate.ToShortDateString())).Returns(true);
            mockParser.Setup(x => x.ParseHourAnswer(ph1.Time.ToLongTimeString())).Returns(true);

            var mockScreeningRoomService = new Mock<IScreeningRoomService>();
            mockScreeningRoomService.Setup(x => x.CheckIfCinemaHasAnyScreeningRoomAsync(programme.Cinema))
                .ReturnsAsync(true);

            var programmeService = new ProgrammeService(mockParser.Object, mockScreeningRoomService.Object);

            //Act
            var result = programmeService.CheckIfProgrammeHasAllNecessaryProperties(programme);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void GetNextStartDate_ValidInput_ValidOutput()
        {
            //Arrange
            var date = new DateTime(2019, 04, 16, 18, 00, 00);
            var programmeService = new ProgrammeService(null, null);

            //Act
            var result = programmeService.GetNextStartDate(date);

            //Assert
            Assert.AreEqual(DayOfWeek.Tuesday, result.DayOfWeek);
        }
    }
}