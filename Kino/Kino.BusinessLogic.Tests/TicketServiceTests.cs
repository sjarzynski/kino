﻿using Kino.BusinessLogic.Helpers;
using Kino.BusinessLogic.Models;
using NUnit.Framework;

namespace Kino.BusinessLogic.Tests
{
    [TestFixture]
    public class TicketServiceTests
    {
        [Test]
        public void DuplicateTicket_InputCinemaName_OutputCinemaName()
        {
            //Arrange
            AutoMapperConfig.CreateMap();
            var cinemaBl = new CinemaBl
            {
                Name = "Krewetka"
            };

            var ticketBl = new TicketBl
            {
                Cinema = cinemaBl                
            };

            var ticketService = new TicketService(null, null, null, null, null, null);

            //Act
            var result = ticketService.DuplicateTicket(ticketBl);

            //Assert
            Assert.AreEqual("Krewetka", result.Cinema.Name);
        }

        [Test]
        public void SumPrices_ValidInput_ValidInput()
        {
            //Arrange
            double sum = 40;
            double singlePrice = 20;

            var ticketService = new TicketService(null, null, null, null, null, null);

            //Act
            var result = ticketService.SumTicketsPrice(sum, singlePrice);

            //Assert
            Assert.AreEqual(60, result);
        }
    }
}
