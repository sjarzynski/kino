﻿using Kino.BusinessLogic.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Kino.BusinessLogic.Tests
{
    [TestFixture]
    public class DiscountServiceTests
    {
        [Test]
        public void ApplyNthTicketDiscount_ValidInput_DiscountOff()
        {
            //Arrange
            var tickets = new List<TicketBl>();
            var ticket1 = new TicketBl();
            var ticket2 = new TicketBl();
            var ticket3 = new TicketBl();
            var ticket4 = new TicketBl();
            var ticket5 = new TicketBl();
            var ticket6 = new TicketBl();

            tickets.Add(ticket1);
            tickets.Add(ticket2);
            tickets.Add(ticket3);
            tickets.Add(ticket4);
            tickets.Add(ticket5);
            tickets.Add(ticket6);

            var price = 20;

            var discountService = new DiscountService();

            //Act
            var result = discountService.ApplyDiscountToTenthTicket(price, tickets);

            //Assert
            Assert.AreEqual(20, result);
        }

        [Test]
        public void ApplyNthTicketDiscount_ValidInput_DiscountOn()
        {
            //Arrange
            var tickets = new List<TicketBl>();
            var ticket1 = new TicketBl();
            var ticket2 = new TicketBl();
            var ticket3 = new TicketBl();
            var ticket4 = new TicketBl();
            var ticket5 = new TicketBl();
            var ticket6 = new TicketBl();
            var ticket7 = new TicketBl();
            var ticket8 = new TicketBl();
            var ticket9 = new TicketBl();

            tickets.Add(ticket1);
            tickets.Add(ticket2);
            tickets.Add(ticket3);
            tickets.Add(ticket4);
            tickets.Add(ticket5);
            tickets.Add(ticket6);
            tickets.Add(ticket7);
            tickets.Add(ticket8);
            tickets.Add(ticket9);

            var price = 20;

            var discountService = new DiscountService();

            //Act
            var result = discountService.ApplyDiscountToTenthTicket(price, tickets);

            //Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void ApplyMonthlyDiscount_ValidInput_DiscountOff()
        {
            //Arrange
            var tickets = new List<TicketBl>();
            var ticket1 = new TicketBl();
            ticket1.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket2 = new TicketBl();
            ticket2.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket3 = new TicketBl();
            ticket3.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket4 = new TicketBl();
            ticket4.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            tickets.Add(ticket1);
            tickets.Add(ticket2);
            tickets.Add(ticket3);
            tickets.Add(ticket4);

            var price = 20;

            var discountService = new DiscountService();

            //Act
            var result = discountService.ApplyMonthlyDiscount(price, tickets);

            //Assert
            Assert.AreEqual(20, result);
        }

        [Test]
        public void ApplyMonthlyDiscount_ValidInput_DiscountOn()
        {
            //Arrange
            var tickets = new List<TicketBl>();
            var ticket1 = new TicketBl();
            ticket1.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket2 = new TicketBl();
            ticket2.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket3 = new TicketBl();
            ticket3.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket4 = new TicketBl();
            ticket4.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var ticket5 = new TicketBl();
            ticket5.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            tickets.Add(ticket1);
            tickets.Add(ticket2);
            tickets.Add(ticket3);
            tickets.Add(ticket4);
            tickets.Add(ticket5);

            var price = 20;

            var discountService = new DiscountService();

            //Act
            var result = discountService.ApplyMonthlyDiscount(price, tickets);

            //Assert
            Assert.AreEqual(16, result);
        }
    }
}
