import { Component, OnInit } from '@angular/core';
import { CinemaItem } from 'src/models/cinemaItem';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cinema-list',
  templateUrl: './cinema-list.component.html',
  styleUrls: ['./cinema-list.component.css']
})
export class CinemaListComponent implements OnInit {

  public cinema:CinemaItem[];

  constructor(private http:HttpClient, private toastr:ToastrService) { }

  ngOnInit() : void {
    this.getCinemas();
  }

  private getCinemas():void{
    this.http.get<CinemaItem[]>('http://localhost:9000/api/cinema')
    .subscribe(
      x=>this.cinema = x,
      ()=>this.toastr.error('Could not download cinemas!', 'Failure')
    )
  }
}
