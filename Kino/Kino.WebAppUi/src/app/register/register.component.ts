import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from 'src/models/customer';
import { HttpClient } from '@angular/common/http';
import { Md5 } from 'ts-md5';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public firstName:string;
  public lastName:string;
  public eMail:string;
  public password:string;
  public confirmPassword:string;

  constructor(
    private toastr:ToastrService,
    private http:HttpClient,
    private router: Router
  ) {}

  ngOnInit() {
  }

  registerCustomer():void{
    if(this.password != this.confirmPassword){
      this.toastr.error('Passwords do not match!', 'Failure')
      return;
    }

    let customer: Customer = {
      FirstName: this.firstName,
      LastName: this.lastName,
      Email: this.eMail,
      PasswordHash: Md5.hashStr(this.password).toString()
    }

    let url:string;
    url = 'http://localhost:9000/api/customer';

    this.http.post(url,customer)
      .subscribe(
        ()=>{
          this.router.navigateByUrl('/');
          this.toastr.success('New customer registered!', 'Success');          
        },
        ()=>this.toastr.error('Cannot register new customer!', 'Failure')
      );
  }
}
