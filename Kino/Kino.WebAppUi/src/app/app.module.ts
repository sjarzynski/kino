import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';
import { CinemaListComponent } from './cinema-list/cinema-list.component';
import { ToastrModule } from 'ngx-toastr';
import {StorageServiceModule} from 'angular-webstorage-service';
import { LocalStorageService } from './services/localStorageService';
import { TicketsComponent } from './tickets/tickets.component';
import { AuthGuard } from './services/auth.guard';
import { ProgrammeDetailsComponent } from './programme-details/programme-details.component';

const appRoutes : Routes =[
  {path: 'register', component: RegisterComponent, pathMatch: 'full'},
  {path: 'cinema-list', component: CinemaListComponent, pathMatch: 'full'},
  
  {path: 'programme-details', children:[
    {path: ':name', component: ProgrammeDetailsComponent, pathMatch: 'full'},
  ]},
  {path: 'tickets', component: TicketsComponent, canActivate:[AuthGuard], pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'home', component: HomeComponent, pathMatch: 'full'},
  {path: '**', component: HomeComponent, pathMatch: 'full'},
]

@NgModule({
  declarations: [
    LoginComponent,
    AppComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent,
    RegisterComponent,
    CinemaListComponent,
    TicketsComponent,
    ProgrammeDetailsComponent
  ],
  imports: [
    StorageServiceModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}
    )
  ],
  providers: [
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
