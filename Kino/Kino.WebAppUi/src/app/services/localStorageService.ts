import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Customer } from 'src/models/customer';

@Injectable()
export class LocalStorageService{

    private credentialsKey = 'banktransfersui_credentials'

    constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService){
    }

    saveCredentials(credentials : Customer) : void{
        this.storage.set(this.credentialsKey, credentials)
    }

    readCredentials(): Customer{
        var credentials: Customer = this.storage.get(this.credentialsKey);
        return credentials;
    }

}