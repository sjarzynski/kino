import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
}                           from '@angular/router';
import { LocalStorageService } from './localStorageService';
import { Customer } from 'src/models/customer';

@Injectable({
  providedIn: 'root',
})

export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private localStorageService:LocalStorageService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.isUserLoggedIn(state);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  private isUserLoggedIn(state: RouterStateSnapshot):boolean{
    var credentials:Customer = this.localStorageService.readCredentials();
    
    if(credentials === null){
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

    return true;
  }
}