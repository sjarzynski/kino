import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Md5 } from 'ts-md5/dist/md5';
import { LocalStorageService } from '../services/localStorageService';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/models/customer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login: string;
  public password: string;

  constructor(private http:HttpClient,
    private toastr:ToastrService,
    private localStorageService: LocalStorageService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
  }

  public performLogin():void{
    if(this.login === undefined || this.password == undefined){
      this.toastr.error('Please provide login and password', 'Failure');
      return;
    }
    
    let customer :Customer={     
      Email: this.login,
      PasswordHash: Md5.hashStr(this.password).toString(),
      FirstName: undefined,
      LastName: undefined
    }

    this.http.post<boolean>('http://localhost:9000/api/customer?email='+ this.login, customer)
    .subscribe(
        x=> this.logonCallback(x, customer),
        () => this.toastr.error('Login failed', 'Failure')
    );
  }

  public performLogout():void{
    this.localStorageService.saveCredentials(null);
    this.toastr.success('Logout successful', 'Success');
    this.router.navigateByUrl('/');
  }

  private logonCallback(success: boolean, credentials: Customer) : void{
    if(success) {
      this.toastr.success('Hello!', 'Success');
      this.localStorageService.saveCredentials(credentials);
      let returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigateByUrl(returnUrl);
    }
    else{
      this.toastr.error('Invalid credentials', 'Failure')
    }
  }
  public loginOnEnter(event) : void{
    if(event.key === "Enter"){
      this.performLogin();
    }
  }

  public isUserLoggedIn():boolean{
    return this.localStorageService.readCredentials() !== null;
  }
}

