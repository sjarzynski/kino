import { Component, OnInit, ModuleWithComponentFactories } from '@angular/core';
import { Programme } from 'src/models/programme';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MockNgModuleResolver } from '@angular/compiler/testing';
import * as moment from 'moment';

@Component({
  selector: 'app-programme-details',
  templateUrl: './programme-details.component.html',
  styleUrls: ['./programme-details.component.css']
})
export class ProgrammeDetailsComponent implements OnInit {

  public programme:Programme[];
  private cName:string;

  constructor(private http:HttpClient, private route:ActivatedRoute, private toastr:ToastrService) { }

  ngOnInit() {
    let theDate = moment().format('YYYY-MM-DD');
    this.cName = this.route.snapshot.paramMap.get('name');
    this.getProgrammes(this.cName, theDate);
  }

  private getProgrammes(cinemaName:string, theDate:string):void{
    this.http.get<Programme[]>('http://localhost:9000/api/programme?date=' + theDate + '&cinemaName=' + cinemaName)
    .subscribe(
      x=>{
        this.programme = x;
        this.toastr.success('Cinema\'s programmes downloaded successfully!', 'Success');
      },
      ()=>this.toastr.error('No programme downloaded!', 'Failure')
    )
  }
}
