import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../services/localStorageService';
import { Customer } from 'src/models/customer';
import { TicketPage } from 'src/models/ticketPage';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  public ticket:TicketPage = new TicketPage();
  private customer: Customer = new Customer();

  constructor(private http:HttpClient, private localStorageService:LocalStorageService, private toastr:ToastrService) { }

  ngOnInit() :void {
    this.customer = this.localStorageService.readCredentials()
    this.getTicketsPage();
  }
  
  public showPrevPage():void{
    this.getTicketsPage(this.ticket.PrevPageAttributes);
  }

  public showNextPage():void{
    this.getTicketsPage(this.ticket.NextPageAttributes);
  }

  private getTicketsPage(urlAttributes:string = ''):void{
    this.http.get<TicketPage>('http://localhost:9000/api/ticket?login=' + this.customer.Email + urlAttributes)
    .subscribe(
      x=>this.ticket = x,
      ()=>this.toastr.error('Could not get the list!', 'Failure')
    )
  }

  public getPageIndexes():number[]{
    let indexes:number[] = [];
    for(let i =1; i<=this.ticket.PagesCount;i++){
      indexes.push(i);
    }

    return indexes;
  }

  public goToPage(page:number):void{
    this.getTicketsPage('&page=' + page + '&pageSize=' + this.ticket.PageSize);
  }
}
