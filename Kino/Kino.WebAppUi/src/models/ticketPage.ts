export class TicketPage{
    public Page:number;
    public PageSize:number;
    public ResultsCount: number;
    public PagesCount: number;
    public NextPageAttributes: string;
    public PrevPageAttributes: string;
    public Results : TicketPage[];
}