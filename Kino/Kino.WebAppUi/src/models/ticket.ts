import { CinemaItem } from './cinemaItem';
import { MovieItem } from './movieItem';
import { PlayingHour } from './playingHour';
import { ScreeningRoom } from './screeningRoom';

export class Ticket{
    Date:Date
    Price:number;
    Row:number;
    Seat:number;
    Cinema:CinemaItem;
    Movie:MovieItem;
    PlayingHour:PlayingHour;
    ScreeningRoom:ScreeningRoom;
}