import { ScreeningRoom } from './screeningRoom';
import { PlayingHour } from './playingHour';

export class PlayingHoursScreeningRoom{
    ScreeningRoom:ScreeningRoom;
    PlayingHours:PlayingHour[];
}