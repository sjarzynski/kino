import { CinemaItem } from './cinemaItem';
import { MovieItem } from './movieItem';
import { PlayingHoursScreeningRoom } from './playingHoursScreeningRoom';

export class Programme{
    StartDate:Date
    Cinema:CinemaItem;
    Movie:MovieItem;
    PlayingHoursScreeningRooms:PlayingHoursScreeningRoom[];
}