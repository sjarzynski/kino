﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class MovieController : ApiController
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        public async Task<object> Post(MovieBl movie)
        {
            var isCorrect = _movieService.CheckIfMovieHasAllNecessaryProperties(movie);
            if (!isCorrect)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var doesExist = await _movieService.CheckIfMovieExistsInDatabaseAsync(movie.Title);
            if (doesExist)
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var movieId = await _movieService.AddMovieAsync(movie.Title, movie.Description, movie.MovieGenre.ToString(),
                movie.Runtime,
                movie.WebUrl);
            return new {id = movieId};
        }
    }
}