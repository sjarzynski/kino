﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.AppWebApi.Interfaces;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi.Helpers
{
    public class ControllersHelper : IControllersHelper
    {
        private readonly ICustomerService _customerService;
        private readonly ITicketService _ticketService;
        private readonly IMailSender _mailSender;
        private readonly IProgrammeService _programmeService;
        private readonly IParser _parser;
        private readonly ICinemaService _cinemaService;
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly IMovieService _movieService;

        public ControllersHelper(ICustomerService customerService,
            ITicketService ticketService,
            IMailSender mailSender,
            IProgrammeService programmeService,
            IParser parser,
            ICinemaService cinemaService,
            IScreeningRoomService screeningRoomService,
            IMovieService movieService)
        {
            _customerService = customerService;
            _ticketService = ticketService;
            _mailSender = mailSender;
            _programmeService = programmeService;
            _parser = parser;
            _cinemaService = cinemaService;
            _screeningRoomService = screeningRoomService;
            _movieService = movieService;
        }

        public async Task<object> CustomerControllerPost(CustomerBl customerBl)
        {
            bool isCorrect = _customerService.CheckIfCustomerHasAllNecessaryProperties(customerBl);
            if (!isCorrect)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            bool doesExist = await _customerService.CheckIfCustomerExistsInDatabaseAsync(customerBl);
            if (doesExist)
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var customerId = await _customerService.AddCustomerAsync(customerBl);
            return new { id = customerId };
        }

        public async Task<List<TicketBl>> TicketControllerGet(CustomerBl customerBl)
        {
            var tickets = (await _ticketService.GetAllTicketsOfSingleCustomerAsync(customerBl.Email)).ToList();
            return tickets;
        }

        public async Task<object> TicketControllerPost(List<TicketBl> ticketsBl)
        {
            double sumOfPrices = 0;

            foreach (var ticketBl in ticketsBl)
            {
                var isCorrect = _ticketService.CheckIfTicketHasAllNecessaryProperties(ticketBl);
                if (!isCorrect)
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                var doesExist = await _ticketService.CheckIfTicketExistAsync(ticketBl);
                if (doesExist)
                    throw new HttpResponseException(HttpStatusCode.Forbidden);

                ticketBl.Price = _ticketService.GetTicketPrice(ticketBl);
                sumOfPrices = _ticketService.SumTicketsPrice(sumOfPrices, ticketBl.Price);

                var ticketId = await _ticketService.AddTicketAsync(ticketBl);
            }

            _mailSender.SendMail(ticketsBl, sumOfPrices);
            return HttpStatusCode.OK;
        }

        public async Task<IEnumerable<ProgrammeBl>> ProgrammeControllerGet(DateTime date, string cinemaName)
        {
            var startDate = _parser.GetStartDate(date);
            if (startDate == DateTime.MinValue) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var doesCinemaExist = await _cinemaService.CheckIfCinemaExistsInDatabaseAsync(cinemaName);
            if (!doesCinemaExist)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var cinema = new CinemaBl { Name = cinemaName };
            var programmes = await _programmeService.GetProgrammesForCinemaAsync(startDate, cinema);
            if (!programmes.Any())
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return programmes;
        }

        public async Task<object> ProgrammeControllerPost(ProgrammeBl programme)
        {
            var isProgrammeCorrect = _programmeService.CheckIfProgrammeHasAllNecessaryProperties(programme);
            if (!isProgrammeCorrect)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            programme.Cinema = await _cinemaService.GetCinemaAsync(programme.Cinema.Name);

            var isCorrectDay = _parser.CheckIfDateIsFriday(programme.StartDate.ToShortDateString());
            if (!isCorrectDay)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var doesCinemaExist = await _cinemaService.CheckIfCinemaExistsInDatabaseAsync(programme.Cinema.Name);
            if (!doesCinemaExist)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var doesMovieExist = await _movieService.CheckIfMovieExistsInDatabaseAsync(programme.Movie.Title);
            if (!doesMovieExist)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            foreach (var playingHoursScreeningRoom in programme.PlayingHoursScreeningRooms)
            {
                var doesScreeningRoomExistInCinema =
                    await _screeningRoomService.CheckIfThisScreeningRoomExistInCinema(programme.Cinema, playingHoursScreeningRoom.ScreeningRoom.Number);
                if (!doesScreeningRoomExistInCinema)
                    throw new HttpResponseException(HttpStatusCode.Forbidden);

                foreach (var playingHour in playingHoursScreeningRoom.PlayingHours)
                {
                    var isTooEarly = _programmeService.CheckIfHourIsTooEarlyOrTooLate(programme.Cinema, playingHour);
                    if (isTooEarly) throw new HttpResponseException(HttpStatusCode.Forbidden);

                    var isHourOccupied = _programmeService.CheckIfHourIsOccupied(programme,
                        playingHoursScreeningRoom.ScreeningRoom, playingHour);
                    if (isHourOccupied) throw new HttpResponseException(HttpStatusCode.Forbidden);
                }
            }

            var programmeId = await _programmeService.AddProgrammeAsync(programme);
            return new { id = programmeId };
        }
    }
}
