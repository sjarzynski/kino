﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class ScreeningRoomController : ApiController
    {
        private readonly IScreeningRoomService _screeningRoomService;
        private readonly ICinemaService _cinemaService;

        public ScreeningRoomController(IScreeningRoomService screeningRoomService, ICinemaService cinemaService)
        {
            _screeningRoomService = screeningRoomService;
            _cinemaService = cinemaService;
        }

        public async Task<object> Post([FromUri] string name, ScreeningRoomBl screeningRoom)
        {
            if (!_screeningRoomService.CheckIfScreeningRoomHasAllNecessaryProperties(screeningRoom))
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var cinema = new CinemaBl {Name = name};
            var doesCinemaExist = await _cinemaService.CheckIfCinemaExistsInDatabaseAsync(cinema.Name);
            if(!doesCinemaExist)
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var doesScreeningRoomExistInCinema = await _screeningRoomService.CheckIfThisScreeningRoomExistInCinema(cinema, screeningRoom.Number);
            if (doesScreeningRoomExistInCinema)
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var screeningRoomId = await _screeningRoomService.AddScreeningRoomAsync(cinema, screeningRoom.Number,
                screeningRoom.Rows, screeningRoom.SeatsInRow);

            return new {id = screeningRoomId};
        }
    }
}