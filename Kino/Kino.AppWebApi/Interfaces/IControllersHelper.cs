﻿using Kino.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.AppWebApi.Interfaces
{
    public interface IControllersHelper
    {
        Task<object> CustomerControllerPost(CustomerBl customerBl);
        Task<List<TicketBl>> TicketControllerGet(CustomerBl customerBl);
        Task<object> TicketControllerPost(List<TicketBl> ticketsBl);
        Task<IEnumerable<ProgrammeBl>> ProgrammeControllerGet(DateTime date, string cinemaName);
        Task<object> ProgrammeControllerPost(ProgrammeBl programme);
    }
}
