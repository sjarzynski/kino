﻿using System.Collections.Generic;
using Kino.AppWebApi.Helpers;
using Kino.AppWebApi.Interfaces;
using Kino.BusinessLogic;
using Kino.BusinessLogic.Helpers;
using Ninject;
using Ninject.Modules;

namespace Kino.AppWebApi
{
    public class NinjectBootstrap
    {
        public IKernel GetKernel()
        {
            AutoMapperConfig.CreateMap();
            var kernel = new StandardKernel();

            kernel.Bind<IControllersHelper>().To<ControllersHelper>();

            kernel.Load(new List<NinjectModule>
            {
                new BusinessLogicModule()
            });

            return kernel;
        }
    }
}