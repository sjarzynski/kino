﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing.Constraints;
using Kino.AppWebApi.Interfaces;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class TicketController : ApiController
    {
        private readonly IControllersHelper _controllersHelper;
        private readonly ITicketService _ticketService;

        public TicketController(IControllersHelper controllersHelper, ITicketService ticketService)
        {
            _controllersHelper = controllersHelper;
            _ticketService = ticketService;
        }

        public PagedResults<TicketBl> Get([FromUri] string login, [FromUri] int pageSize = 10, [FromUri] int page = 1)
        {
            return _ticketService.GetTicketsHistoryPage(login, pageSize, page);
        }

        public async Task<List<TicketBl>> Get([FromUri]string eMail)
        {
            var customerBl = new CustomerBl
            {
                Email = eMail
            };

            var tickets = await _controllersHelper.TicketControllerGet(customerBl);
            return tickets;
        }

        public async Task<object> Post(List<TicketBl> ticketsBl)
        {
            var result = await _controllersHelper.TicketControllerPost(ticketsBl);
            return result;
        }
    }
}
