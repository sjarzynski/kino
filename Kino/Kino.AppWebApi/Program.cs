﻿using System;
using Topshelf;

namespace Kino.AppWebApi
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>
            {
                x.Service<OwinBootstrap>(s =>
                {
                    s.ConstructUsing(name => new OwinBootstrap());
                    s.WhenStarted(owinBootstrap => owinBootstrap.Start());
                    s.WhenStopped(owinBootstrap => owinBootstrap.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Kino server WebApi Service");
                x.SetDisplayName("KinoWebApi");
                x.SetServiceName("KinoWebApi");
            });

            var exitCode = (int) Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}