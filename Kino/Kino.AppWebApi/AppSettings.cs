﻿using System.Configuration;

namespace Kino.AppWebApi
{
    internal interface IAppSettings
    {
        string WebApiUri { get; }
    }

    internal class AppSettings : IAppSettings
    {
        public string WebApiUri => ConfigurationManager.AppSettings["WebApiUri"];
    }
}