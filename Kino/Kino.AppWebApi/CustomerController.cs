﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.AppWebApi.Interfaces;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class CustomerController : ApiController
    {
        private readonly IControllersHelper _controllersHelper;
        private readonly ICustomerService _customerService;

        public CustomerController(IControllersHelper controllersHelper, ICustomerService customerService)
        {
            _controllersHelper = controllersHelper;
            _customerService = customerService;
        }

        public async Task<object> Post(CustomerBl customerBl)
        {
            var result = await _controllersHelper.CustomerControllerPost(customerBl);
            return result;
        }

        public bool Post([FromUri] string eMail, CustomerBl customerBl)
        {
            return _customerService.IsValidCustomer(eMail, customerBl);
        }
    }
}
