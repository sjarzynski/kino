﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.AppWebApi.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class ProgrammeController : ApiController
    {
        private readonly IControllersHelper _controllersHelper;

        public ProgrammeController(IControllersHelper controllersHelper)
        {
            _controllersHelper = controllersHelper;
        }

        public async Task<IEnumerable<ProgrammeBl>> Get([FromUri] DateTime date, [FromUri] string cinemaName)
        {
            var programmes = await _controllersHelper.ProgrammeControllerGet(date, cinemaName);
            return programmes;
        }

        public async Task<object> Post(ProgrammeBl programme)
        {
            var result = await _controllersHelper.ProgrammeControllerPost(programme);
            return result;
        }
    }
}