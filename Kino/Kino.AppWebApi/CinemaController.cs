﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Kino.BusinessLogic.Interfaces;
using Kino.BusinessLogic.Models;

namespace Kino.AppWebApi
{
    public class CinemaController : ApiController
    {
        private readonly ICinemaService _cinemaService;

        public CinemaController(ICinemaService cinemaService)
        {
            _cinemaService = cinemaService;
        }

        public async Task<IEnumerable<CinemaBl>> Get()
        {
            var cinemas = await _cinemaService.GetAllCinemasAsync();
            return cinemas;
        }

        public async Task<object> Post(CinemaBl cinema)
        {
            var isCorrect = _cinemaService.CheckIfCinemaHasAllNecessaryProperties(cinema);
            if (!isCorrect)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var doesExist = await _cinemaService.CheckIfCinemaExistsInDatabaseAsync(cinema.Name);
            if (doesExist)
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var cinemaId =
                await _cinemaService.AddCinemaAsync(cinema.Name, cinema.Address, cinema.Email, cinema.PhoneNumber,
                    cinema.OpenHours, cinema.CloseHours);
            return new {id = cinemaId};
        }
    }
}