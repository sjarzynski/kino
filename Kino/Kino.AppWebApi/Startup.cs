﻿using System.Web.Http;
using System.Web.Http.Cors;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace Kino.AppWebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}/",
                new {id = RouteParameter.Optional}
            );

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            appBuilder
                .UseNinjectMiddleware(new NinjectBootstrap().GetKernel)
                .UseNinjectWebApi(config);
        }
    }
}